#include "GenericBot.h"
#include <vector>

#include <direct.h>
#include<iostream>
#include <stdio.h> 

GenericBot::GenericBot(string fileName, std::vector<Tool> *tools){// std::vector<Eigen::Matrix4f> frames) {




	getBot(fileName, tools);

	size = this->joints.size();
	//translate(*this->activeTool->getFrame() , *this->joints.at(size - 1).getFrame());

	totDist = this->joints.at(size - 1).getFrame()->row(0)(3) * this->joints.at(size - 1).getFrame()->row(0)(3) +
				this->joints.at(size - 1).getFrame()->row(1)(3) * this->joints.at(size - 1).getFrame()->row(1)(3) +
				this->joints.at(size - 1).getFrame()->row(2)(3) * this->joints.at(size - 1).getFrame()->row(2)(3);

	//totDist = this->frameList.at(size - 1)(0, 3) * this->frameList.at(size - 1)(0, 3) +
	//			this->frameList.at(size - 1)(1, 3) * this->frameList.at(size - 1)(1, 3) +
	//			this->frameList.at(size - 1)(2, 3) * this->frameList.at(size - 1)(2, 3);

	totDist = sqrt(totDist);
}

//GenericBot& GenericBot::operator=(const GenericBot anotherBot) {
//	//this->frameList = anotherBot.frameList;
//	return *this;
//}

bool GenericBot::moveJointsAbsolute(std::vector<float> ang)
{
	if (ang.size() != size) { return false; }
	static std::vector<float > adjustAng(size);

	for (int i = 0; i < size; i++) {
		//adjustAng.at(i) = ang.at(i) - jntAngles.at(i);
		adjustAng.at(i) = ang.at(i) - *this->joints.at(i).getAngle();

		//jntAngles.at(i) = ang.at(i);
		this->joints.at(i).setNewAngle(ang.at(i));
	}
	this->updateJoints(0,adjustAng);

	return true;

}


bool GenericBot::moveJointAbsolute(int index, float ang) {
	if (index >= size) { return false; }
	float adjustAng;

	//adjustAng = ang - jntAngles.at(index);
	adjustAng = ang - *this->joints.at(index).getAngle();
	//jntAngles.at(index) = ang;
	this->joints.at(index).setNewAngle(ang);

	updateJoint(index, adjustAng);
	return true;
}

bool GenericBot::moveJointRelative(int index, float ang)
{
	if (index >= size) { return false; }
	//jntAngles.at(index) += ang;
	this->joints.at(index).relativeAngMove(ang);
	this->updateJoint(index, ang);
	return true;
}


bool GenericBot::moveJointsRelative(std::vector<float> ang)
{
	if (ang.size() != size) { return false; }
	for (int i = 0; i < size; i++) {
		//jntAngles.at(i) += ang.at(i);
		this->joints.at(i).relativeAngMove(ang.at(i));
	}
	this->updateJoints(0, ang);
	return true;
}



std::vector<float> GenericBot::getJntAngles(void) {
	std::vector<float> angles = std::vector<float>();
	//assuming 0 articulation to base...
	angles.push_back(0);
	for (int i = 1; i < this->joints.size(); i++) {

		//Eigen::Vector3f xAxis0 = Eigen::Vector3f(frameList.at(i - 1)(0, 0), frameList.at(i - 1)(1, 0), frameList.at(i - 1)(2, 0));
		//Eigen::Vector3f xAxis1 = Eigen::Vector3f(frameList.at(i)(0, 0), frameList.at(i)(1, 0), frameList.at(i)(2, 0));

		Eigen::Vector3f xAxis0 = Eigen::Vector3f(this->joints.at(i - 1).getFrame()->row(0)(0), this->joints.at(i - 1).getFrame()->row(1)(0), this->joints.at(i - 1).getFrame()->row(2)(0));
		Eigen::Vector3f xAxis1 = Eigen::Vector3f(this->joints.at(i).getFrame()->row(0)(0), this->joints.at(i).getFrame()->row(1)(0), this->joints.at(i).getFrame()->row(2)(0));

		Eigen::Vector3f xAxissCr = xAxis0.cross(xAxis1);
		//Eigen::Vector4f targetPiv = frameList.at(i) * framePivotList.at(i);
		Eigen::Vector4f targetPiv = *this->joints.at(i).getFrame() * this->joints.at(i).getFrame()->col(2);


		float mag = xAxissCr.dot(Eigen::Vector3f(targetPiv(0), targetPiv(1), targetPiv(2)));

		float ang = pcl::getAngle3D(xAxis0, xAxis1);
		ang *= mag < 0 ? -1 : 1;

		angles.push_back(ang);
		int hete = 345;
	}
	return angles;
}

const std::vector<pcl::PolygonMesh>* GenericBot::getMeshs(void) {
	//frameMeshList.at(0).cloud.
	//pcl::transformPointCloud(frameMeshList.at(0).cloud, frameMeshList.at(0).cloud, frameList.at(0),);

	Eigen::Affine3f move = Eigen::Affine3f::Identity();
	move.translation() << 0, 0, 0;
	float z = 0;
	for (int i = 0; i < size; i++) {

		//if (i == size - 2) {
		//	if (!activeTool.isSTLAvail()) {
		//		//no stl for active tool. 
		//		break;
		//	}
		//}

		//Eigen::Matrix4f tempFrame = *this->joints.at(i).getFrame();// frameList.at(i);

		//Eigen::Affine3f move = Eigen::Affine3f::Identity();
		//move.translation() << frameList.at(i)(0,3), frameList.at(i)(1, 3), frameList.at(i)(2, 3);
		pcl::PointCloud<pcl::PointXYZ> cloud;
		pcl::fromPCLPointCloud2(goldenFrameMeshList.at(i).cloud, cloud);
		pcl::transformPointCloud(cloud, cloud, *this->joints.at(i).getFrame());
		pcl::toPCLPointCloud2(cloud, frameMeshList.at(i).cloud);
	}

	return &this->frameMeshList;
}


const pcl::PolygonMesh* GenericBot::getMesh(int index) {
	//frameMeshList.at(0).cloud.
	//pcl::transformPointCloud(frameMeshList.at(0).cloud, frameMeshList.at(0).cloud, frameList.at(0),);

	Eigen::Affine3f move = Eigen::Affine3f::Identity();
	move.translation() << 0, 0, 0;
	float z = 0;

		//Eigen::Matrix4f tempFrame = frameList.at(index);
		Eigen::Matrix4f tempFrame = *this->joints.at(index).getFrame();


		//Eigen::Affine3f move = Eigen::Affine3f::Identity();
		//move.translation() << frameList.at(i)(0,3), frameList.at(i)(1, 3), frameList.at(i)(2, 3);
		pcl::PointCloud<pcl::PointXYZ> cloud;
		pcl::fromPCLPointCloud2(goldenFrameMeshList.at(index).cloud, cloud);
		pcl::transformPointCloud(cloud, cloud, tempFrame);
		pcl::toPCLPointCloud2(cloud, frameMeshList.at(index).cloud);

	return &this->frameMeshList.at(index);
}

//const std::vector<Eigen::Matrix4f>* GenericBot::getFrames(void) {
//	return &this->frameList;
//}

const Eigen::Matrix4f * GenericBot::getJointFrame(int index)
{
	if (index >= size) { return nullptr; }
	return this->joints.at(index).getFrame();
}

bool GenericBot::getJointFrameCopy(int index, Eigen::Matrix4f * targetFrame) {

	if (index >= this->getSize()) { return false; }

	targetFrame = &this->joints.at(index).getFrameRef();

	return true;
}

Eigen::Matrix4f GenericBot::getBotEndFrame(void)
{
	return this->joints.at(this->size - 1).getFrameRef();
}


//const std::vector<Eigen::Matrix4f>* GenericBot::getGoldenFrames(void) {
//	return &this->goldenFrameList;
//}

//int GenericBot::getFrame(int frameNum, const Eigen::Matrix4f* targFrame) {
//	if (frameNum >= this->frameList.size()) { return -1; }
//	targFrame = &this->frameList.at(frameNum);
//	return 0;
//}

/**
*	\brief used to update the current active tool
*	\param Tool setAsActiveTool tool that should be considered on end of tool
*	\return bool returns true input param is considered as the active tool
*/
//bool GenericBot::setActiveTool(Tool setAsActiveTool)
//{
//	for (int i = 0; i < availableTools.size(); i++) {
//		if (availableTools.at(i) == setAsActiveTool) {
//			activeTool = setAsActiveTool;
//			return true;
//		}
//	}
//
//	return false;
//}

const Eigen::Matrix4f * GenericBot::getActiveToolFrame(void)
{
	return this->activeTool->getFrame();
}

Eigen::Matrix4f GenericBot::getToolFrameCopy(void)
{
	return this->activeTool->getToolFrameRef();
}


void GenericBot::attachTool(Tool *newActiveTool)
{
	this->activeTool = newActiveTool;
	VoyagerMath::translate(*this->activeTool->getFrame() , *this->joints.at(size - 1).getFrame());
}

Tool * GenericBot::getToolByName(string toolName, bool wasToolAvail)
{
	
	for (int i = 0; i < availableTools.size(); i++) {
		if (toolName.compare(availableTools.at(i).name())) {
			wasToolAvail = true;
			return &availableTools.at(i);
		}
	}
	//if the tool was not found a generic null tool
	//is returned. 
	wasToolAvail = false;
	return &Tool();

}

/*
void GenericBot::rotateAny(Eigen::Matrix4f & inframe, Eigen::RowVector3f pnt, Eigen::RowVector3f dir, double ang)
{
	Eigen::Matrix4f transformation = Eigen::Matrix4f();

	dir.normalize();

	float u = dir(0);
	float v = dir(1);
	float w = dir(2);

	float a = pnt.x();
	float b = pnt.y();
	float c = pnt.z();

	transformation(0, 0) = u * u + (v * v + w * w) * cos(ang);
	transformation(0, 1) = u * v * (1 - cos(ang)) - w * sin(ang);
	transformation(0, 2) = u * w * (1 - cos(ang)) + v * sin(ang);
	transformation(0, 3) = (a * (v * v + w * w) - u * (b * v + c * w)) * (1 - cos(ang)) + (b * w - c * v) * sin(ang);

	transformation(1, 0) = u * v * (1 - cos(ang)) + w * sin(ang);
	transformation(1, 1) = v * v + (u * u + w * w)*cos(ang);
	transformation(1, 2) = v * w * (1 - cos(ang)) - u * sin(ang);
	transformation(1, 3) = (b * (u * u + w * w) - v * (a * u + c * w)) * (1 - cos(ang)) + (c * u - a * w) * sin(ang);

	transformation(2, 0) = u * w * (1 - cos(ang)) - v * sin(ang);
	transformation(2, 1) = v * w * (1 - cos(ang)) + u * sin(ang);
	transformation(2, 2) = w * w + (u * u + v * v)*cos(ang);
	transformation(2, 3) = (c * (u * u + v * v) - w * (a * u + b * v)) * (1 - cos(ang)) + (a * v - b * u) * sin(ang);

	transformation(3, 0) = 0;
	transformation(3, 1) = 0;
	transformation(3, 2) = 0;
	transformation(3, 3) = 1;

	inframe = transformation * inframe;
}
*/
/*
void GenericBot::translate(Eigen::Matrix4f & inframe, Eigen::Vector3f trans)
{
	inframe(0, 3) += trans(0);
	inframe(1, 3) += trans(1);
	inframe(2, 3) += trans(2);
}

void GenericBot::translate(Eigen::Matrix4f & inframe, Eigen::Matrix4f bytrans)
{
	inframe(0, 3) += bytrans(0, 3);
	inframe(1, 3) += bytrans(1, 3);
	inframe(2, 3) += bytrans(2, 3);
}
*/
void GenericBot::updateJoints(int startFrame, const std::vector<float> ang)
{
	//Eigen::RowVector4f dir;
	for (int i = 0; i < size; i++) {
updateJoint(i, ang.at(i));
	}
}

void GenericBot::updateJoint(int trgJoint, float ang)
{
	Eigen::RowVector4f dir;
	for (int ii = trgJoint; ii < size; ii++) {

		dir = this->joints.at(trgJoint).getFrame()->col(2);

		VoyagerMath::rotateAny(*this->joints.at(ii).getFrame(),
			Eigen::RowVector3f(this->joints.at(trgJoint).getFrame()->row(0)(3), this->joints.at(trgJoint).getFrame()->row(1)(3), this->joints.at(trgJoint).getFrame()->row(2)(3)),
			Eigen::RowVector3f(dir(0), dir(1), dir(2)),
			ang);

	}
	VoyagerMath::rotateAny(*this->activeTool->getFrame(),
		Eigen::RowVector3f(this->joints.at(trgJoint).getFrame()->row(0)(3), this->joints.at(trgJoint).getFrame()->row(1)(3), this->joints.at(trgJoint).getFrame()->row(2)(3)),
		Eigen::RowVector3f(dir(0), dir(1), dir(2)),
		ang);
}

int GenericBot::getSize(void) {
	return this->size;
}

float GenericBot::getTotDist(void) {
	return this->totDist;
}
std::vector<Collider> GenericBot::getColliders(void) {
	std::vector<Collider> colliders = std::vector<Collider>();
	for (int i = 0; i < this->joints.size(); i++) {
		colliders.push_back(*this->joints.at(i).getCollider());
	}
	return colliders;
}

void GenericBot::getBot(string filename, std::vector<Tool> *tools) {

	//this->frameList.clear();
	//this->framePivotList.clear();
	this->frameMeshList.clear();
	//std::vector<Eigen::Matrix4f> genBotFrames = std::vector<Eigen::Matrix4f>();
	//std::vector<Eigen::Vector4f> genBotPivot = std::vector<Eigen::Vector4f>();
	//std::vector<pcl::PolygonMesh> genBotSTLs = std::vector<pcl::PolygonMesh>();

	string line;
	vector<string> lines = vector<string>();
	vector<string> comments = vector<string>();
	bool stripOtherTags = true;
	bool wasNullToolSet = false;
	cout << workingdir() << endl;




	char tuh[_MAX_PATH];
	char *abc;
	abc = getcwd(tuh, sizeof(tuh));

	ifstream myfile("iiwa.genBot");

	Eigen::Matrix4f tmpFrame;
	Eigen::Vector4f tmpPivot;
	std::vector<float> configAng;
	std::vector<float> configCollider;

	int count = 0;

	// try parsing myfile to generate generic bot
	try {
		Eigen::Vector3f reorientVect = Eigen::Vector3f(0, 0, 0);
		if (myfile)
		{
			while (getline(myfile, line))
			{
				count++;
				if (line.find("#") != std::string::npos || line.length() == 0) {
					comments.push_back(line);
				}
				else {
					lines.push_back(line);
					if (line.find("frame=") != std::string::npos) {
						tmpFrame = generateFrame(line.substr(6));
						//this->frameList.push_back(tmpFrame);
						reorientVect += Eigen::Vector3f(tmpFrame(0, 3), tmpFrame(1, 3), tmpFrame(2, 3));
						tmpFrame(0, 3) = reorientVect(0);
						tmpFrame(1, 3) = reorientVect(1);
						tmpFrame(2, 3) = reorientVect(2);
						if (!getline(myfile, line)) {
							break;
						}
						count++;
						if (line.find("angle=") != std::string::npos) {
							configAng = getConfigAngles(line.substr(6));
							if (configAng.size() != 3) {
								throw std::runtime_error("num angles for this line not correct");
							}
						}
						else {
							throw std::runtime_error("configure file error associated with angle");
						}

						if (!getline(myfile, line)) {
							break;
						}
						count++;
						pcl::PointNormal tempPNorm = pcl::PointNormal();
						Eigen::Vector3f position = VoyagerMath::getWAxis(&tmpFrame);
						tempPNorm.x = position.x();
						tempPNorm.y = position.y();
						tempPNorm.z = position.z();
						tempPNorm.normal_x = 0;
						tempPNorm.normal_y = 0;
						tempPNorm.normal_z = 1;

						if (line.find("collider=sphere,") != std::string::npos) {
							configCollider = this->getFloatVect(line.substr(16));
							if (configCollider.size() != 1) {
								throw std::runtime_error("Spherical collider config not correct");
							}

							joints.push_back(
								Joint(tmpFrame,
									configAng.at(0),
									configAng.at(1),
									configAng.at(2),
									Collider(configCollider.at(0), tempPNorm)));

						}else if(line.find("collider=cylinder,") != std::string::npos){
							configCollider = this->getFloatVect(line.substr(18));
							if (configCollider.size() != 3) {
								throw std::runtime_error("Cylindrical collider config not correct");
							}
							
							bool isBiDirectional = configCollider.at(2) == 0 ? false : true;

							joints.push_back(
								Joint(tmpFrame,
									configAng.at(0),
									configAng.at(1),
									configAng.at(2),
									Collider(configCollider.at(0), configCollider.at(1), isBiDirectional, tempPNorm)));
			;
						}else {
							throw std::runtime_error("configure file error associated with collider type");
						}

						count++;
						if (!getline(myfile, line)) {
							break;
						}

						if (line.find("model=") != std::string::npos) {

							this->frameMeshList.push_back(generateMesh(line.substr(6), tmpFrame));
						}
					}
					if (line.find("ToolList") != std::string::npos) {
						string toolName;
						Matrix4f toolFrame = Matrix4f::Identity();
						PolygonMesh toolMesh;
						bool parseTools = true;
						getline(myfile, line);
						count++;
						while (parseTools) {
							if (line.find("EndToolList") != std::string::npos) { break; }
							if (line.find("name=") != std::string::npos) {
								toolName = line.substr(5);
							}
							else { break; }
							if (!getline(myfile, line)) {
								break; }
							count++;
							if (line.find("frame=") != std::string::npos) {
								toolFrame = generateFrame(line.substr(6));
							}
							else { break; }
							if (!getline(myfile, line)) {
								break; 
							}
							count++;

							if (line.find("model=") != std::string::npos) {
								toolMesh = generateMesh(line.substr(6), toolFrame);
								//availableTools.push_back(Tool(toolName, toolFrame, toolMesh));
								//toolBox->push_back(Tool(toolName, toolFrame, toolMesh));
								//toolBox->addTool(Tool(toolName, toolFrame, toolMesh));
								tools->push_back(Tool(toolName, toolFrame, toolMesh));
							}
							else {
								//availableTools.push_back(Tool(toolName, toolFrame));
								//toolBox->push_back(Tool(toolName, toolFrame));
								tools->push_back(Tool(toolName, toolFrame));
								//toolBox->addTool(Tool(toolName, toolFrame));

							}
							//was a null tool given in the config file...
							if (!toolName.compare("null_tool")) {
								wasNullToolSet = true;
							}
							toolName = "void";
							toolFrame = Matrix4f::Identity();
						}
					}
				}
			}
			myfile.close();
		}
	}
	catch (const std::exception& error) {
		std::cerr << "File parsing Error line " << count << ": " << error.what() << std::endl;
	}
	if (!wasNullToolSet) {
		tools->push_back(Tool());
	}

	//this->frameList = genBotFrames;
	//this->framePivotList = genBotPivot;
	//this->goldenFrameList = this->frameList;// genBotFrames;
	//this->frameMeshList = genBotSTLs;
	this->goldenFrameMeshList = this->frameMeshList;// genBotSTLs;
}

std::string GenericBot::workingdir(void)
{
	char buf[256];
	GetCurrentDirectoryA(256, buf);
	return std::string(buf) + '\\';
}

std::vector<float> GenericBot::getFloatVect(std::string stlLine)
{
	float i;
	vector<float> vect = vector<float>();
	int cnt = 0;
	std::stringstream ss(stlLine);

	while (ss >> i)
	{
		vect.push_back(i);

		if (ss.peek() == ',')
			ss.ignore();
		cnt++;
	}
	return vect;
}

Eigen::Matrix4f GenericBot::generateFrame(std::string frameLine) {

	Eigen::Matrix4f thisFrame = Eigen::Matrix4f::Identity();
	std::vector<float> vectFloats = this->getFloatVect(frameLine);

	for (int cnt = 0; cnt < vectFloats.size(); cnt++) {
		thisFrame(cnt / 4, cnt % 4) = vectFloats.at(cnt);
	}
	return thisFrame;
}

Eigen::Vector4f GenericBot::generatePivot(std::string stlLine) {
	/**/
	std::stringstream ss(stlLine);

	//float i;
	Eigen::Vector4f vect = Eigen::Vector4f::Zero();
	std::vector<float> vectFloats = this->getFloatVect(stlLine);

	for (int cnt = 0; cnt < vectFloats.size(); cnt++) {
		vect(cnt) = vectFloats.at(cnt);
	}
	return vect;
}

std::vector<float> GenericBot::getConfigAngles(std::string stlLine)
{
	return this->getFloatVect(stlLine);
}

pcl::PolygonMesh GenericBot::generateMesh(std::string stlLine, Eigen::Matrix4f tmpFrame) {

	static string pwd = workingdir();
	string tempPWD = pwd;

	pcl::PolygonMesh tempMesh;
	tempPWD.append(stlLine);
	if (stlLine.compare("null") == 0) {
		tempMesh = pcl::PolygonMesh::PolygonMesh();
		pcl::PointCloud<pcl::PointXYZ> cloud;
		cloud.push_back(pcl::PointXYZ(0, 0, 0));
		pcl::toPCLPointCloud2(cloud, tempMesh.cloud);
		int here = 234;
		return tempMesh;
	}
	else {
		pcl::io::loadPolygonFileSTL(tempPWD, tempMesh);
	}
	//Eigen::Matrix4f moveToOrigin = Eigen::Matrix4f::Identity();
	//moveToOrigin(0, 3) = -tmpFrame(0, 3);
	//moveToOrigin(1, 3) = -tmpFrame(1, 3);
	//moveToOrigin(2, 3) = -tmpFrame(2, 3);
	
	Eigen::Matrix4f moveToOrigin = tmpFrame.inverse();
	

	pcl::PointCloud<pcl::PointXYZ> cloud;
	pcl::fromPCLPointCloud2(tempMesh.cloud, cloud);
	pcl::transformPointCloud(cloud, cloud, moveToOrigin);
	pcl::toPCLPointCloud2(cloud, tempMesh.cloud);

	return tempMesh;
}