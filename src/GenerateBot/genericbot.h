
#include <vector>
#include <string>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>
#include <pcl/io/vtk_lib_io.h> 

#include <VoyagerMath.h>
#include <ToolBox.h>
#include <Joint.h>

using namespace std;

#ifndef GENERIC_BOT_H
#define GENERIC_BOT_H 

class GenericBot {

public:
	GenericBot(string fileName, std::vector<Tool> *toolBox);// std::vector<Eigen::Matrix4f> frames);
	//Link& operator=(const Link& anotherLink);
	//GenericBot& operator=(const GenericBot anotherBot);

	bool GenericBot::moveJointsAbsolute(std::vector<float> ang);
	bool GenericBot::moveJointsRelative(std::vector<float> ang);
	bool GenericBot::moveJointAbsolute(int index, float ang);
	bool GenericBot::moveJointRelative(int index, float ang);



	std::vector<float> GenericBot::getJntAngles(void);
	const std::vector<pcl::PolygonMesh>* GenericBot::getMeshs(void); //jnt Class
	const pcl::PolygonMesh* GenericBot::getMesh(int index); //jnt Class
	//const std::vector<Eigen::Matrix4f>* getFrames(void);
	const Eigen::Matrix4f* getJointFrame(int index);
	/**
	*	returns false if joint index is not accessible. 
	*/
	bool GenericBot::getJointFrameCopy(int index, Eigen::Matrix4f * targetFrame);
	Eigen::Matrix4f GenericBot::getBotEndFrame(void);
	const std::vector<Joint>* getJoints();

	//const std::vector<Eigen::Matrix4f>* getGoldenFrames(void);
	//int getFrame(int frameNum, const Eigen::Matrix4f* targFrame);
	//bool setActiveTool(Tool activeTool);
	const Eigen::Matrix4f * GenericBot::getActiveToolFrame(void);
	Eigen::Matrix4f GenericBot::getToolFrameCopy(void);
	void attachTool(Tool *newActiveTool);

	int getSize(void); //getJointCount()
	float getTotDist(void);
	std::vector<Collider> getColliders(void);

	std::vector<Joint> joints;

private:
	GenericBot() {};
	//std::vector<Eigen::Matrix4f> frameList; 	//jnt Class
	//std::vector<float> jntAngles;				//jnt Class



	//std::vector<Eigen::Matrix4f> goldenFrameList; //remove/consolidate
	//std::vector<Eigen::Vector4f> framePivotList;	//remove/consolidate
	std::vector<pcl::PolygonMesh> frameMeshList;
	std::vector<pcl::PolygonMesh> goldenFrameMeshList; //remove/consolidate
	std::vector<Tool> availableTools;			// tool box
	Tool *activeTool;

	Tool * getToolByName(string toolName, bool wasToolAvail);
	//void rotateAny(Eigen::Matrix4f &inframe, Eigen::RowVector3f pnt, Eigen::RowVector3f dir, double ang);
	//void translate(Eigen::Matrix4f &inframe, Eigen::Vector3f trans);
	//void translate(Eigen::Matrix4f &inframe, Eigen::Matrix4f  bytrans);
	void updateJoints(int startFrame, const std::vector<float> ang);
	void updateJoint(int trgJoint, float ang);


	void getBot(string fileName, std::vector<Tool> *toolBox);
	Eigen::Matrix4f generateFrame(std::string frameLine);
	pcl::PolygonMesh generateMesh(std::string stlLine, Eigen::Matrix4f reorientVect);
	Eigen::Vector4f generatePivot(std::string stlLine);
	std::vector<float> getConfigAngles(std::string stlLine);

	std::string workingdir(void);

	std::vector<float> getFloatVect(std::string stlLine);

	int size;
	float totDist;
};

#endif