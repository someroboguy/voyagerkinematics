#include "VoyagerMath.h"

void VoyagerMath::rotateAny(Eigen::Matrix4f & inframe, Eigen::RowVector3f pnt, Eigen::RowVector3f dir, double ang)
{
	Eigen::Matrix4f transformation = Eigen::Matrix4f();

	dir.normalize();

	float u = dir(0);
	float v = dir(1);
	float w = dir(2);

	float a = pnt.x();
	float b = pnt.y();
	float c = pnt.z();

	transformation(0, 0) = u * u + (v * v + w * w) * cos(ang);
	transformation(0, 1) = u * v * (1 - cos(ang)) - w * sin(ang);
	transformation(0, 2) = u * w * (1 - cos(ang)) + v * sin(ang);
	transformation(0, 3) = (a * (v * v + w * w) - u * (b * v + c * w)) * (1 - cos(ang)) + (b * w - c * v) * sin(ang);

	transformation(1, 0) = u * v * (1 - cos(ang)) + w * sin(ang);
	transformation(1, 1) = v * v + (u * u + w * w)*cos(ang);
	transformation(1, 2) = v * w * (1 - cos(ang)) - u * sin(ang);
	transformation(1, 3) = (b * (u * u + w * w) - v * (a * u + c * w)) * (1 - cos(ang)) + (c * u - a * w) * sin(ang);

	transformation(2, 0) = u * w * (1 - cos(ang)) - v * sin(ang);
	transformation(2, 1) = v * w * (1 - cos(ang)) + u * sin(ang);
	transformation(2, 2) = w * w + (u * u + v * v)*cos(ang);
	transformation(2, 3) = (c * (u * u + v * v) - w * (a * u + b * v)) * (1 - cos(ang)) + (a * v - b * u) * sin(ang);

	transformation(3, 0) = 0;
	transformation(3, 1) = 0;
	transformation(3, 2) = 0;
	transformation(3, 3) = 1;

	inframe = transformation * inframe;
}

Eigen::Vector3f VoyagerMath::rodriques(Eigen::Vector3f & vectorToRotate, Eigen::Vector3f & aboutAxis, float ang)
{
	aboutAxis.normalize();
	return vectorToRotate * cos(ang) + aboutAxis.cross(vectorToRotate)*sin(ang) + aboutAxis * (aboutAxis.dot(vectorToRotate))*(1-cos(ang));
}

void VoyagerMath::translate(Eigen::Matrix4f &inframe, Eigen::Vector3f trans) 
{
	inframe(0, 3) += trans(0);
	inframe(1, 3) += trans(1);
	inframe(2, 3) += trans(2);
}
void VoyagerMath::translate(Eigen::Matrix4f &inframe, Eigen::Matrix4f  bytrans) 
{
	inframe(0, 3) += bytrans(0, 3);
	inframe(1, 3) += bytrans(1, 3);
	inframe(2, 3) += bytrans(2, 3);
}

void VoyagerMath::translate(pcl::PointXYZ & inPnt, Eigen::Vector3f byTrans)
{
	inPnt.x += byTrans.x();
	inPnt.y += byTrans.y();
	inPnt.z += byTrans.z();
}

Eigen::Vector3f VoyagerMath::moveTo(Eigen::Matrix4f &fromFrame, const Eigen::Matrix4f *moveTo) {
	Eigen::Vector3f moveBy = Eigen::Vector3f(moveTo->col(3)(0) - fromFrame.col(3)(0),
										     moveTo->col(3)(1) - fromFrame.col(3)(1),
										     moveTo->col(3)(2) - fromFrame.col(3)(2));
	VoyagerMath::translate(fromFrame, moveBy);
	return moveBy;
}

float VoyagerMath::angleTo(const Eigen::Vector3f * from, const Eigen::Vector3f * to, Eigen::Vector3f & rotAxis)
{
	rotAxis = from->cross(*to);
	if (rotAxis.x() == 0 && rotAxis.y() == 0 && rotAxis.z() == 0) {
		
		if (from->dot(*to) >= 0) { 
			rotAxis.x() = 0;
			rotAxis.y() = 0;
			rotAxis.z() = 0;
			return 0; 
		}
		//setting rotation axis to axis perpendicular to 
		// the 'to' axis
		rotAxis.x() = to->z();
		rotAxis.y() = to->z();
		rotAxis.z() = -to->x() - to->y();
		return M_PI;
	}
	rotAxis.normalize();

	return pcl::getAngle3D(*from, *to);
}

pcl::PointXYZ VoyagerMath::getPointAlongLine(Eigen::Vector3f * dir, float dist, pcl::PointXYZ fromPnt)
{
	dir->normalize();
	pcl::PointXYZ returnPnt = fromPnt;

	returnPnt.x += dir->x() * dist;
	returnPnt.y += dir->y() * dist;
	returnPnt.z += dir->z() * dist;

	return returnPnt;
}

pcl::PointXYZ VoyagerMath::getPointAlongLine(const pcl::PointNormal * dirPntNorm, float dist)
{
	return VoyagerMath::getPointAlongLine(
		&Eigen::Vector3f(dirPntNorm->normal_x, dirPntNorm->normal_y, dirPntNorm->normal_z),
		dist,
		pcl::PointXYZ(dirPntNorm->x, dirPntNorm->y, dirPntNorm->z));
}

Eigen::Vector3f VoyagerMath::getXAxis(const Eigen::Matrix4f * fromThisMatrix)
{
	return Eigen::Vector3f(fromThisMatrix->col(0)(0), fromThisMatrix->col(0)(1), fromThisMatrix->col(0)(2));
}
Eigen::Vector3f VoyagerMath::getYAxis(const Eigen::Matrix4f * fromThisMatrix)
{
	return Eigen::Vector3f(fromThisMatrix->col(1)(0), fromThisMatrix->col(1)(1), fromThisMatrix->col(1)(2));
}
Eigen::Vector3f VoyagerMath::getZAxis(const Eigen::Matrix4f * fromThisMatrix)
{
	return Eigen::Vector3f(fromThisMatrix->col(2)(0), fromThisMatrix->col(2)(1), fromThisMatrix->col(2)(2));
}
Eigen::Vector3f VoyagerMath::getWAxis(const Eigen::Matrix4f * fromThisMatrix)
{
	return Eigen::Vector3f(fromThisMatrix->col(3)(0), fromThisMatrix->col(3)(1), fromThisMatrix->col(3)(2));
}

pcl::PointXYZ VoyagerMath::getPointXYZ(const Eigen::Matrix4f * fromThisMatrix)
{
	return pcl::PointXYZ(fromThisMatrix->col(3)(0), fromThisMatrix->col(3)(1), fromThisMatrix->col(3)(2));
}

Eigen::Vector3f VoyagerMath::dirToPoint(const pcl::PointXYZ *from, const pcl::PointXYZ *to) {
	Eigen::Vector3f dir = Eigen::Vector3f(to->x - from->x, to->y - from->y, to->z - from->z);
	dir.normalize();
	return dir;
}

Eigen::Vector3f VoyagerMath::vectToPoint(const pcl::PointXYZ * from, const pcl::PointXYZ * to)
{
	return Eigen::Vector3f(to->x - from->x, to->y - from->y, to->z - from->z);
}

Eigen::Vector3f VoyagerMath::vectToPoint(const Eigen::Vector3f * from, const Eigen::Vector3f * to)
{
	return Eigen::Vector3f(to->x() - from->x(), to->y() - from->y(), to->z() - from->z());
}



double VoyagerMath::squareDist(const pcl::PointXYZ * from, const pcl::PointXYZ * to)
{
	Eigen::Vector3f v2Pnt = vectToPoint(from, to);
	return v2Pnt.x() * v2Pnt.x() + v2Pnt.y() * v2Pnt.y() + v2Pnt.z() * v2Pnt.z();
}

double VoyagerMath::squareDist(const pcl::PointXYZ * from, const pcl::PointNormal * to)
{
	return VoyagerMath::squareDist(from, &pcl::PointXYZ(to->x, to->y, to->z));
}

double VoyagerMath::squareDist(const Eigen::Vector3f vect)
{
	return vect.x() * vect.x() + vect.y() * vect.y() + vect.z() * vect.z();
}

double VoyagerMath::squareDistFromLine(const pcl::PointXYZ * atPnt, pcl::PointNormal fromLine)
{
	Eigen::Vector3f axis(fromLine.normal_x, fromLine.normal_y, fromLine.normal_z);
	axis.normalize();
	Eigen::Vector3f toPoint = 
		VoyagerMath::vectToPoint(&pcl::PointXYZ(fromLine.x, fromLine.y, fromLine.z), atPnt);
	return VoyagerMath::squareDist(axis.cross(toPoint));
}

double VoyagerMath::squareDistFromLine(const pcl::PointXYZ * atPnt, pcl::PointNormal fromLine, Eigen::Vector3f * toPerpLinePnt)
{
	Eigen::Vector3f axis(fromLine.normal_x, fromLine.normal_y, fromLine.normal_z);
	axis.normalize();
	Eigen::Vector3f toPoint =
		VoyagerMath::vectToPoint(&pcl::PointXYZ(fromLine.x, fromLine.y, fromLine.z), atPnt);
	Eigen::Vector3f toPntCrossAxis = toPoint.cross(axis);
	double squaredDist = VoyagerMath::squareDist(toPntCrossAxis);
	Eigen::Vector3f tempTo(0, 0, 0);
	// if point was not on line...
	if (squaredDist != 0) {
		//getting vector that points to perpendicular point on line from atPnt
		Eigen::Vector3f tempTo = toPntCrossAxis.cross(axis);
		tempTo.normalize();
		toPerpLinePnt->x() = tempTo.x();
		toPerpLinePnt->y() = tempTo.y();
		toPerpLinePnt->z() = tempTo.z();
	}
	return squaredDist;
}

double VoyagerMath::distFromLine(const pcl::PointXYZ * atPnt, pcl::PointNormal fromLine, Eigen::Vector3f * fromLinePntToPnt, pcl::PointXYZ *pntOnLine)
{
	Eigen::Vector3f axis(fromLine.normal_x, fromLine.normal_y, fromLine.normal_z);
	axis.normalize();
	Eigen::Vector3f toPoint =
		VoyagerMath::vectToPoint(&pcl::PointXYZ(fromLine.x, fromLine.y, fromLine.z), atPnt);
	Eigen::Vector3f toPntCrossAxis = toPoint.cross(axis);
	double squaredDist = VoyagerMath::squareDist(toPntCrossAxis);
	Eigen::Vector3f tempTo(0, 0, 0);
	// if point was not on line...
	if (squaredDist != 0) {
		//getting vector that points to perpendicular point on line from atPnt
		Eigen::Vector3f tempTo = axis.cross(toPntCrossAxis);
		tempTo.normalize();
		fromLinePntToPnt->x() = tempTo.x();
		fromLinePntToPnt->y() = tempTo.y();
		fromLinePntToPnt->z() = tempTo.z();
	}
	double dist = sqrt(squaredDist);
	pntOnLine->x = atPnt->x + dist * -fromLinePntToPnt->x();
	pntOnLine->y = atPnt->y + dist * -fromLinePntToPnt->y();
	pntOnLine->z = atPnt->z + dist * -fromLinePntToPnt->z();
	return dist;
}

Eigen::Vector3f VoyagerMath::pntOnArbCircle(const double * rad, const pcl::PointXYZ * origPnt, pcl::PointNormal * fromLine, Eigen::Vector3f * toPerpLinePnt)
{
	Eigen::Vector3f linePntToTargPnt(0, 0, 0);
	pcl::PointXYZ pntOnLine(0, 0, 0);
	double dist = VoyagerMath::distFromLine(origPnt, *fromLine, &linePntToTargPnt, &pntOnLine);


	fromLine->x = pntOnLine.x;
	fromLine->y = pntOnLine.y;
	fromLine->z = pntOnLine.z;

	Eigen::Vector3f orbitPnt(fromLine->x, fromLine->y, fromLine->z);
	Eigen::Vector3f orbitVect(fromLine->normal_x, fromLine->normal_y, fromLine->normal_z);


	return dist * cos(*rad)*linePntToTargPnt + dist * sin(*rad)*orbitVect.cross(linePntToTargPnt) + orbitPnt;
}






