
#include <pcl/common/common_headers.h>

#ifndef VOYAGER_MATH_H
#define VOYAGER_MATH_H

static class VoyagerMath {

public:

	static void rotateAny(Eigen::Matrix4f & inframe, Eigen::RowVector3f pnt, Eigen::RowVector3f dir, double ang);

	/**
	*	\brief rotates vector about arbitrary vector 
	*	\param vectorToRotate the vector to rotate
	*	\param aboutAxis axis vectorToRotate is rotated about
	*	\param float ang angle in radians
	*	
	*/
	static Eigen::Vector3f rodriques(Eigen::Vector3f & vectorToRotate, Eigen::Vector3f &aboutAxis, float ang);

	static void translate(Eigen::Matrix4f &inframe, Eigen::Vector3f trans);
	static void translate(Eigen::Matrix4f &inframe, Eigen::Matrix4f  bytrans);
	static void translate(pcl::PointXYZ &inPnt, Eigen::Vector3f byTrans);
	static Eigen::Vector3f moveTo(Eigen::Matrix4f &fromFrame, const Eigen::Matrix4f *moveTo);
	/**
	*	\brief gives the angle and direction to get to a vector from a starting vector
	*	\param Eigen::Vector3f *from is the starting vector
	*	\param Eigen::Vector3f *to is the finishing (or target) vector orientation
	*	\param Eigen::Vector3f *rotAxis value is updated to reflect the axis that should be rotated<br>
	*	&emsp;about by the return angle to get to the finishing(or target) vector orientation.<br>
	*	\return float the angle in radians that need to be rotate to get to get to the target<br>
	*	&emsp;vector orientation
	*
	*
	*	for non 0 or M_PI return values the rotAxis returned value will be the cross product to X from
	*	for returned value of 0 the rotAxis is set to all zeros.
	*	for returned value of M_PI the rotAxis is set to vector perpedicular to 'to' 
	*/
	static float angleTo(const Eigen::Vector3f *from, const Eigen::Vector3f *to, Eigen::Vector3f &rotAxis);

	/**
	*	\brief gives a point that is a givance distance from a point in a direction NOTE: *dir will be normalized
	*	\param Eigen::Vector3f *dir the direction in which the returned point will be derived
	*	\param float dist distance along *dir from 'fromPnt' to determine the return point
	*	\param pcl::PointXYZ fromPnt is the reference point
	*	\return pcl::PointXYZ a point in space that is 'dist' from 'fromPnt' in the direction of '*dir'
	*
	*/
	static pcl::PointXYZ getPointAlongLine(Eigen::Vector3f *dir, float dist, pcl::PointXYZ fromPnt);
	static pcl::PointXYZ getPointAlongLine(const pcl::PointNormal *dirPntNorm, float dist);

	static Eigen::Vector3f getXAxis(const Eigen::Matrix4f * fromThisMatrix);
	static Eigen::Vector3f getYAxis(const Eigen::Matrix4f * fromThisMatrix);
	static Eigen::Vector3f getZAxis(const Eigen::Matrix4f * fromThisMatrix);
	static Eigen::Vector3f getWAxis(const Eigen::Matrix4f * fromThisMatrix);
	/**
	*	\brief treats the input Matrix4f as a rotation matrix and returns the a point<br>
	*	&emsp;associated with column 3 row 0 through 2.<br>
	*
	*/
	static pcl::PointXYZ getPointXYZ(const Eigen::Matrix4f * fromThisMatrix);
	/**
	*	\brief gives normalized direction to point from another point
	*/
	static Eigen::Vector3f dirToPoint(const pcl::PointXYZ *from, const pcl::PointXYZ *to);

	static Eigen::Vector3f vectToPoint(const pcl::PointXYZ *from, const pcl::PointXYZ *to);
	static Eigen::Vector3f vectToPoint(const Eigen::Vector3f *from, const Eigen::Vector3f *to);
	/**
	*	\brief used to return the squared distance between two points. 
	*
	*/
	static double squareDist(const pcl::PointXYZ *from, const pcl::PointXYZ *to);
	/**
	*	\brief used to return the squared distance between two points.
	*
	*/
	static double squareDist(const pcl::PointXYZ * from, const pcl::PointNormal * to);
	/**
	*	\brief returns the squared distance of the input vector3f
	*/
	static double squareDist(const Eigen::Vector3f vect);

	/**
	*	\brief used to return the squared distance between a point and a line
	*	\param const pcl::PointXYZ *atPnt point to get distance from "fromLine"
	*	\param pcl::PointNormal fromLine line to get distance from "atPnt"
	*/
	static double squareDistFromLine(const pcl::PointXYZ *atPnt, pcl::PointNormal fromLine);

	/**
	*	\brief used to return the squared distance between a point and a line
	*	\param const pcl::PointXYZ *atPnt point to get distance from "fromLine"
	*	\param pcl::PointNormal fromLine line to get distance from "atPnt"
	*	\param Eigen::Vector3f *toPerpLinePnt is the perpedicular normalized vector from atPnt to fromLine
	*/
	static double squareDistFromLine(const pcl::PointXYZ *atPnt, pcl::PointNormal fromLine, Eigen::Vector3f * toPerpLinePnt);
	/**
	*	\brief used to return the (non-squared)distance between a point and a line
	*	\param const pcl::PointXYZ *atPnt point to get distance from "fromLine"
	*	\param pcl::PointNormal fromLine line to get distance from "atPnt"
	*	\param Eigen::Vector3f *fromLinePntToPnt is the perpedicular normalized vector from pnt on "fromLine" to atPnt
	*	\param pcl::PointXYZ *pntOnLine is the point on the fromLine that is perpendicular to atPnt
	*/
	static double distFromLine(const pcl::PointXYZ *atPnt, pcl::PointNormal fromLine, Eigen::Vector3f * fromLinePntToPnt, pcl::PointXYZ *pntOnLine);
	/**
	*	\brief given pnt on a circle gives another point rad radians away from that point around some aribrary line
	*/
	static Eigen::Vector3f pntOnArbCircle(const double *rad, const pcl::PointXYZ *origPnt, pcl::PointNormal *fromLine, Eigen::Vector3f *toPerpLinePnt);
};

#endif