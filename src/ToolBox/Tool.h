

#include <pcl/common/transforms.h>
#include <pcl/io/vtk_lib_io.h> 
#include <string>

#ifndef TOOL_H
#define TOOL_H

using namespace Eigen;
using namespace pcl;
using namespace std;



class Tool {

public:
	Tool();
	Tool(string name, Matrix4f toolFrame);
	Tool(string name, Matrix4f toolFrame, PolygonMesh toolMesh);
	Matrix4f * getFrame(void);
	Matrix4f getToolFrameRef(void);
	const string name(void) { return toolName; }
	bool isSTLAvail(void) { return hasSTL; } 
	void setActiveState(bool isActive) { this->isActive = isActive; }

	bool Tool::operator==(const Tool &anotherTool);

private:
	string toolName;
	Matrix4f toolFrame;
	Matrix4f goldenToolFrame;
	PolygonMesh toolMesh;
	bool hasSTL = false;
	bool isActive = false;
};

#endif