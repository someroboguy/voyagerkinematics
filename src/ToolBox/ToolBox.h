#include<Tool.h>
#include <string.h>

#ifndef TOOLBOX_H
#define TOOLBOX_H

class ToolBox {
public:
	ToolBox(std::vector<Tool>* Tools);

	void addTool(Tool tool);
	Tool * setActiveTool(int index);
	Tool * setActiveTool(string toolName);
	int numAvailableTools(void);

	std::vector<Tool> availableTools;

private:


};

#endif // !TOOLBOX_H
