
#include <ToolBox.h>


ToolBox::ToolBox(std::vector<Tool>* Tools) {
	this->availableTools = *Tools;
}

void ToolBox::addTool(Tool tool) {
	
	this->availableTools.push_back(tool);
	//this->availableTools.push_back(tool);
	this;
}

Tool* ToolBox::setActiveTool(int index) {
	if (index < 0 || index > this->numAvailableTools()) { 
		return nullptr; 
	}
	for (int i = 0; i < this->numAvailableTools(); i++) {
		if (i != index) {
			this->availableTools.at(i).setActiveState(false);
		}
		else {
			this->availableTools.at(i).setActiveState(true);
		}
	}
	//returns pointer to active tool. 
	return &this->availableTools.at(index);
}

Tool * ToolBox::setActiveTool(string toolName)
{
	for (int i = 0; i < availableTools.size(); i++) {
		if (availableTools.at(i).name().find(toolName) != std::string::npos) {
			return &availableTools.at(i);
		}
	}
	return nullptr;
}

int ToolBox::numAvailableTools(void)
{
	return 0;
	//return this->availableTools->size();
}
