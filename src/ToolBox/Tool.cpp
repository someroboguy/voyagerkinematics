#include "Tool.h"


Tool::Tool() {
	this->toolName = "null_tool";
	this->toolFrame = Matrix4f::Identity();
	this->toolFrame(2, 3) = 400;
	this->goldenToolFrame = this->toolFrame;
	this->hasSTL = false;
}

Tool::Tool(string name, Matrix4f toolFrame) {
	this->toolName = name;
	this->toolFrame = toolFrame;
	this->goldenToolFrame = toolFrame;
	this->hasSTL = false;
}

Tool::Tool(string name, Matrix4f toolFrame, PolygonMesh toolMesh) {
	this->toolName = name;
	this->toolFrame = toolFrame;
	this->goldenToolFrame = toolFrame;
	this->hasSTL = true;
	this->toolMesh = toolMesh;
}

Matrix4f * Tool::getFrame(void) {
	return &this->toolFrame;
}

Matrix4f Tool::getToolFrameRef(void) {
	return this->toolFrame;
}

bool Tool::operator==(const Tool & anotherTool)
{
	if (anotherTool.toolName != this->toolName) { return false; }
	if (anotherTool.hasSTL != this->hasSTL) { return false; }
	// actual tool frame can be adjusted but the goldenToolFrame must be the same!
	if (anotherTool.goldenToolFrame != this->goldenToolFrame) { return false; }
	if (anotherTool.hasSTL) {
		if (anotherTool.toolMesh.header.frame_id != this->toolMesh.header.frame_id) { return false; }
	}
	return true;
}

