#include "FABRIK.h"
#include <ctime>

FABRIK::FABRIK(GenericBot * iiwa)
{
	this->targetBot = iiwa;
	this->internalToolFrame = this->targetBot->getToolFrameCopy();
	this->internalGenBotEnd = this->targetBot->getBotEndFrame();
	this->botFirstLink_Base = VoyagerMath::getPointXYZ(this->targetBot->getJointFrame(2));
	this->linkColliders = this->targetBot->getColliders();

	linkLengths.push_back(getLinkLength(0, 2));
	linkLengths.push_back(getLinkLength(2, 4));
	linkLengths.push_back(getLinkLength(4, 6));
	linkLengths.push_back(getLinkLength(6, 8));

	this->mutableLinkEnd = std::vector<pcl::PointXYZ>();
	this->derivedAngles = std::vector<float>();

	this->derivedAngles.push_back(this->targetBot->joints.at(1).getAngleRef());
	this->derivedAngles.push_back(this->targetBot->joints.at(2).getAngleRef());
	this->derivedAngles.push_back(this->targetBot->joints.at(3).getAngleRef());
	this->derivedAngles.push_back(this->targetBot->joints.at(4).getAngleRef());
	this->derivedAngles.push_back(this->targetBot->joints.at(5).getAngleRef());
	this->derivedAngles.push_back(this->targetBot->joints.at(6).getAngleRef());
	this->derivedAngles.push_back(this->targetBot->joints.at(7).getAngleRef());

	this->tagAlongYAxis = Eigen::Vector3f(1, 0, 0);

	this->captureCurrentMutableLinks();

	maxReach = linkLengths.at(0) + linkLengths.at(1);

}

std::vector<Eigen::Vector3f> FABRIK::getCurrentJointEnds(void)
{
	return std::vector<Eigen::Vector3f>();
}

void FABRIK::spinTool(float radian, Eigen::Matrix4f targFrame)
{
	VoyagerMath::rotateAny(
		internalToolFrame,
		VM::getWAxis(&internalToolFrame),// Eigen::Vector3f(internalToolFrame.col(3)(0), internalToolFrame.col(3)(1), internalToolFrame.col(3)(2)),
		VM::getZAxis(&internalToolFrame),// Eigen::Vector3f(internalToolFrame.col(2)(0), internalToolFrame.col(2)(1), internalToolFrame.col(2)(2)),
		radian
	);

	VoyagerMath::rotateAny(
		internalGenBotEnd,
		VM::getWAxis(&internalToolFrame),// Eigen::Vector3f(internalToolFrame.col(3)(0), internalToolFrame.col(3)(1), internalToolFrame.col(3)(2)),
		VM::getZAxis(&internalToolFrame),// Eigen::Vector3f(internalToolFrame.col(2)(0), internalToolFrame.col(2)(1), internalToolFrame.col(2)(2)),
		radian
	);
	this->botLastLink_Base = VoyagerMath::getPointAlongLine(&VoyagerMath::getZAxis(&internalGenBotEnd),
		-linkLengths.at(linkLengths.size() - 1),
		VoyagerMath::getPointXYZ(&internalGenBotEnd));
	
}

/**
*	\brief overlays the active tool to the target frame
*
*/
void FABRIK::placeTool(Eigen::Matrix4f *targFrame)
{
	this->moveEndAndToolToTarget(targFrame);

	Eigen::Vector3f pivDir(0, 0, 0);
	double ang = VM::angleTo(&VM::getXAxis(&this->internalToolFrame), &VM::getXAxis(targFrame), pivDir);
	ang *= pivDir.dot(VM::getXAxis(targFrame)) < 0 ? -1 : 1;

	spinTool(ang, *targFrame);
}


float FABRIK::getLinkLength(int startJntIndex, int endJntIndex)
{
	Eigen::RowVector4f tmpJntBase = this->targetBot->getJointFrame(startJntIndex)->col(3);
	Eigen::RowVector4f tmpJntEnd = this->targetBot->getJointFrame(endJntIndex)->col(3);

	pcl::PointXYZ base = pcl::PointXYZ(tmpJntBase(0), tmpJntBase(1), tmpJntBase(2));
	pcl::PointXYZ end = pcl::PointXYZ(tmpJntEnd(0), tmpJntEnd(1), tmpJntEnd(2));

return pcl::geometry::distance(base, end);
}

void FABRIK::moveEndAndToolToTarget(Eigen::Matrix4f * targFrame)
{
	// deteriming if the internal Tool frame is already in the correct axial direction
	if (targFrame->col(3)(0) != internalToolFrame.col(3)(0) ||
		targFrame->col(3)(1) != internalToolFrame.col(3)(0) ||
		targFrame->col(3)(2) != internalToolFrame.col(3)(0)) {
		Eigen::Vector3f movedBy = VoyagerMath::moveTo(internalToolFrame, targFrame);
		VoyagerMath::translate(this->internalGenBotEnd, movedBy);
	}
	if (targFrame->col(2)(0) != internalToolFrame.col(2)(0) ||
		targFrame->col(2)(1) != internalToolFrame.col(2)(1) ||
		targFrame->col(2)(2) != internalToolFrame.col(2)(2)) {



		Eigen::Vector3f targetZ = Eigen::Vector3f(targFrame->col(2)(0), targFrame->col(2)(1), targFrame->col(2)(2));
		Eigen::Vector3f currenZ = Eigen::Vector3f(internalToolFrame.col(2)(0), internalToolFrame.col(2)(1), internalToolFrame.col(2)(2));
		Eigen::Vector3f dir(0, 0, 0);

		float ang = VoyagerMath::angleTo(&currenZ, &targetZ, dir);

		if (ang != 0) {
			VoyagerMath::rotateAny(internalToolFrame,
				Eigen::Vector3f(targFrame->col(3)(0), targFrame->col(3)(1), targFrame->col(3)(2)),
				dir,
				ang);
			VoyagerMath::rotateAny(internalGenBotEnd,
				Eigen::Vector3f(targFrame->col(3)(0), targFrame->col(3)(1), targFrame->col(3)(2)),
				dir,
				ang);
		}
	}
	this->botLastLink_Base = VoyagerMath::getPointAlongLine(&VoyagerMath::getZAxis(&internalGenBotEnd),
		-linkLengths.at(linkLengths.size() - 1),
		VoyagerMath::getPointXYZ(&internalGenBotEnd));

}

bool FABRIK::hitAxialSymmetricTargetLinearMove(Eigen::Matrix4f targFrame) {

	this->captureCurrentMutableLinks();
	this->placeTool(&targFrame);

	//this->spinTool(M_PI / 10000, targFrame, true);

	float tempDist = pcl::geometry::distance(this->botLastLink_Base, this->botFirstLink_Base);

	if (tempDist > this->maxReach) {
		//Need to finish... reorient axial symmetric tool if able to minimize reach distance
		return false;
	}

	int cnt = 0;
	std::vector<float> distVec;
	float dist = pcl::geometry::distance(this->botLastLink_Base, this->mutableLinkEnd.at(2));
	distVec.push_back(dist);
	while (dist > MIN_DIST_FROM_TARGET && cnt++ < MAX_FABRIK_ITERATIONS) {
		this->fabrikIteration();
		dist = pcl::geometry::distance(this->botLastLink_Base, this->mutableLinkEnd.at(2));
		distVec.push_back(dist);
	}
	if (cnt >= MAX_FABRIK_ITERATIONS) {
		throw std::runtime_error("FABRIK did not converge to anwer");
	}

	optimizeLinks();

	deriveAngles();	
	return checkAnglesForCompliance();
}

void FABRIK::captureCurrentMutableLinks(void) {
	this->mutableLinkEnd.clear();
	this->mutableLinkEnd.push_back(VoyagerMath::getPointXYZ(this->targetBot->getJointFrame(2)));
	this->mutableLinkEnd.push_back(VoyagerMath::getPointXYZ(this->targetBot->getJointFrame(4)));
	this->mutableLinkEnd.push_back(VoyagerMath::getPointXYZ(this->targetBot->getJointFrame(6)));
}

void FABRIK::deriveAngles(void)
{
	//this->derivedAngles.clear();
	this->tagAlongYAxis = Eigen::Vector3f(1, 0, 0);

	Eigen::Vector3f dir(0, 0, 0);
	Eigen::Vector3f link0Dir = VoyagerMath::dirToPoint(&pcl::PointXYZ(0, 0, 0), &this->mutableLinkEnd.at(0));
	Eigen::Vector3f link1Dir = VoyagerMath::dirToPoint(&this->mutableLinkEnd.at(0), &this->mutableLinkEnd.at(1));
	Eigen::Vector3f link2Dir = VoyagerMath::dirToPoint(&this->mutableLinkEnd.at(1), &this->mutableLinkEnd.at(2));
	Eigen::Vector3f link3Dir = VoyagerMath::dirToPoint(&this->mutableLinkEnd.at(2), &VoyagerMath::getPointXYZ(&this->internalGenBotEnd));

	double distLink0FromLink0Plane = link0Dir.dot(VM::vectToPoint(&this->mutableLinkEnd.at(0), &this->mutableLinkEnd.at(1)));
	pcl::PointXYZ pntOnLink0Plane = VM::getPointAlongLine(&link0Dir, -distLink0FromLink0Plane, this->mutableLinkEnd.at(1));
	Eigen::Vector3f vectToPntOn0Plane = VM::vectToPoint(&this->mutableLinkEnd.at(0), &pntOnLink0Plane);

	Eigen::Vector3f ang0Cross(0, 0, 0);
	double ang0 = VM::angleTo(&this->tagAlongYAxis, &vectToPntOn0Plane, ang0Cross);
	bool flip0 = false;
	//used to prevent joint flipping. 
	if (fabs(ang0 - this->derivedAngles.at(0)) > M_PI - 0.1){
		flip0 = true;
		ang0 += ang0 < 0 ? M_PI : -M_PI;
	}

	double val = ang0Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(1)));
	this->tagAlongYAxis = VM::rodriques(this->tagAlongYAxis, ang0Cross, ang0);
	ang0 *= val < 0 ? -1 : 1;

	Eigen::Vector3f ang1Cross(0, 0, 0);
	double ang1 = VM::angleTo(&link0Dir, &link1Dir, ang1Cross);
	if (flip0) {
	//	ang1 *= -1;
	}
	val = ang1Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(2)));
	this->tagAlongYAxis = VM::rodriques(this->tagAlongYAxis, ang1Cross, ang1);
	ang1 *= val < 0 ? -1 : 1;

	this->derivedAngles.at(0) = ang0;
	this->derivedAngles.at(1) = ang1;

	Eigen::Vector3f elus4 = VM::getZAxis(this->targetBot->getJointFrame(4));

	double distLink2FromLink1Plane = link1Dir.dot(VM::vectToPoint(&this->mutableLinkEnd.at(1), &this->mutableLinkEnd.at(2)));
	pcl::PointXYZ pntOnLink1Plane = VM::getPointAlongLine(&link1Dir, -distLink2FromLink1Plane, this->mutableLinkEnd.at(2));
	Eigen::Vector3f vecToPntOn1Plane = VM::vectToPoint(&this->mutableLinkEnd.at(1), &pntOnLink1Plane);

	Eigen::Vector3f ang2Cross(0, 0, 0);
	double ang2 = VM::angleTo(&this->tagAlongYAxis, &vecToPntOn1Plane, ang2Cross);
	if (flip0) {
	//	ang2 += ang2 < 0 ? M_PI : -M_PI;
	}
	val = ang2Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(3)));
	ang2 *= val < 0 ? -1 : 1;

	Eigen::Vector3f ang3Cross(0, 0, 0);
	double ang3 = VM::angleTo(&link1Dir, &link2Dir, ang3Cross);
	if (flip0) {
	//	ang3 *= -1;
	}
	val = ang3Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(4)));
	ang3 *= val < 0 ? -1 : 1;

	this->tagAlongYAxis = VM::rodriques(this->tagAlongYAxis, ang2Cross, fabs(ang2));
	this->tagAlongYAxis = VM::rodriques(this->tagAlongYAxis, ang3Cross, fabs(ang3));
	this->derivedAngles.at(2) = ang2;
	this->derivedAngles.at(3) = ang3;

	double distFrom = pcl::geometry::distance(this->mutableLinkEnd.at(2), VoyagerMath::getPointXYZ(&this->internalGenBotEnd));
	double distLink3FromLink2Plane = link2Dir.dot(VM::vectToPoint(&this->mutableLinkEnd.at(2), &VoyagerMath::getPointXYZ(&this->internalGenBotEnd)));
	pcl::PointXYZ pntOnLink2Plane = VM::getPointAlongLine(&link2Dir, -distLink3FromLink2Plane, VoyagerMath::getPointXYZ(&this->internalGenBotEnd));
	Eigen::Vector3f vecToPntOn2Plane = VM::vectToPoint(&this->mutableLinkEnd.at(2), &pntOnLink2Plane);

	Eigen::Vector3f ang4Cross(0, 0, 0);
	double ang4 = VM::angleTo(&this->tagAlongYAxis, &vecToPntOn2Plane, ang4Cross);
	val = ang4Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(5)));
	double val2 = ang2Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(4)));
	double val3 = ang2Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(6)));
	
	Eigen::Vector3f Z4 = VM::getZAxis(this->targetBot->getJointFrame(4));
	Eigen::Vector3f Z5 = VM::getZAxis(this->targetBot->getJointFrame(5));
	Eigen::Vector3f Z6 = VM::getZAxis(this->targetBot->getJointFrame(6));
	
	ang4 *= val < -0.001 ? -1 : 1;
	
	Eigen::Vector3f ang5Cross(0, 0, 0);
	double ang5 = VM::angleTo(&link2Dir, &link3Dir, ang5Cross);
	val = ang5Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(6)));
	ang5 *= val < 0 ? -1 : 1;
	
	Eigen::Vector3f elus6 = VM::getZAxis(this->targetBot->getJointFrame(6));
	
	this->tagAlongYAxis = VM::rodriques(this->tagAlongYAxis, ang4Cross, fabs(ang4));
	this->tagAlongYAxis = VM::rodriques(this->tagAlongYAxis, ang5Cross, fabs(ang5));
	this->derivedAngles.at(4) = ang4;
	this->derivedAngles.at(5) = ang5;
	
	Eigen::Vector3f ang6Cross(0, 0, 0);
	double ang6 = VM::angleTo(&this->tagAlongYAxis, &VM::getXAxis(&this->internalGenBotEnd), ang6Cross);
	val = ang6Cross.dot(VM::getZAxis(this->targetBot->getJointFrame(7)));
	ang6 *= val < 0 ? -1 : 1;
	
	this->tagAlongYAxis = VM::rodriques(this->tagAlongYAxis, ang6Cross, fabs(ang6));
	this->derivedAngles.at(6) = ang6;

	//this->updateColliders();
}

bool FABRIK::checkAnglesForCompliance(void)
{
	for (int i = 0; i < this->derivedAngles.size(); i++) {
		double here = *this->targetBot->joints.at(i+1).getMaxAngle();
		if (derivedAngles.at(i) > *this->targetBot->joints.at(i + 1).getMaxAngle() ||
			derivedAngles.at(i) < *this->targetBot->joints.at(i + 1).getMinAngle()) {

			//throw std::runtime_error("Joint out or range");
			int here = 123;
			return false;
		}
	}
	return true;
}

void FABRIK::updateColliders(void)
{
	throw std::runtime_error("updateCollider not implemented");
}

void FABRIK::optimizeLinks(void)
{
	std::vector<pcl::PointXYZ> copyMutLinkEnd = std::vector<pcl::PointXYZ>(this->mutableLinkEnd.size());
	std::copy(this->mutableLinkEnd.begin(), this->mutableLinkEnd.end(), copyMutLinkEnd.begin());


	int here = 123;
	copyMutLinkEnd.at(1) = copyMutLinkEnd.at(2);
	here = 1234;

	throw std::runtime_error("include optimize with pntOnArbCircle");

}

void FABRIK::fabrikIteration(void) {
	//forwardReach
	Eigen::Vector3f dirL1Neg = VoyagerMath::dirToPoint(&this->botLastLink_Base, &this->mutableLinkEnd.at(1));
	this->mutableLinkEnd.at(1) = VoyagerMath::getPointAlongLine(&dirL1Neg, this->linkLengths.at(2), this->botLastLink_Base);

	//backwardReach
	Eigen::Vector3f dirL1Pos = VoyagerMath::dirToPoint(&this->botFirstLink_Base, &this->mutableLinkEnd.at(1));
	this->mutableLinkEnd.at(1) = VoyagerMath::getPointAlongLine(&dirL1Pos, this->linkLengths.at(1), this->botFirstLink_Base);
	Eigen::Vector3f dirL2Pos = VoyagerMath::dirToPoint(&this->mutableLinkEnd.at(1), &this->botLastLink_Base);
	this->mutableLinkEnd.at(2) = VoyagerMath::getPointAlongLine(&dirL2Pos, this->linkLengths.at(2), this->mutableLinkEnd.at(1));
}