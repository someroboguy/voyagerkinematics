
#include <GenericBot.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/geometry.h>
#include <Eigen/Core>
#include <VoyagerMath.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/project_inliers.h>

#ifndef FABRIK_H
#define FABRIK_H

typedef VoyagerMath VM;

class FABRIK {
public:
	FABRIK(GenericBot * genBot);

	std::vector<Eigen::Vector3f> getCurrentJointEnds(void);
	const Eigen::Matrix4f * getInternalToolFrame(void) { return &internalToolFrame; }
	const Eigen::Matrix4f * getInternalGenBotEnd(void) { return &internalGenBotEnd; }
	const pcl::PointXYZ * getEndLinkBase(void) { return &botLastLink_Base; }

	/**
	*	\brief used when tool is considered axial symmetric 
	*	\param target frame to updated
	*	\return bool true if reachable, else false
	*    
	*	This method is used to find the optimal joint position based on the target<br>
	*	&emsp;frame. It assumes that the tool normal is important but takes advantage<br>
	*	&emsp;of tool axial symmetry to minimize jnt articulation. 
	*/
	bool hitAxialSymmetricTargetLinearMove(Eigen::Matrix4f targFrame);

	const std::vector<pcl::PointXYZ> * getFABRIKLinks(void) { return &mutableLinkEnd; }
	float getDerivedAngle(int index) { return derivedAngles.at(index); }
	Eigen::Vector3f gettagAlongYAxis(void) { return tagAlongYAxis; }
	pcl::PointXYZ getGenBotEnd(void) { return VoyagerMath::getPointXYZ(&internalGenBotEnd); }

private:

	const float MIN_DIST_FROM_TARGET = 0.01;
	const int MAX_FABRIK_ITERATIONS = 20;

	void spinTool(float radian, Eigen::Matrix4f targFrame);
	void placeTool(Eigen::Matrix4f *targFrame);
	/**
	*	\brief used to get the current links of interest terminiation points 
	*	\param void
	*	\return void
	*/
	void captureCurrentMutableLinks(void);
	void deriveAngles(void);
	bool checkAnglesForCompliance(void);
	void updateColliders(void);
	/**
	*	\brief give a FABRIK derived frame optimize to account for collisions and joint limitations
	*/
	void optimizeLinks(void);

	//FABRIK() {}
	GenericBot * targetBot;
	std::vector<Collider> linkColliders;
	std::vector<float> linkLengths;
	std::vector<float> derivedAngles; ///< holds the calculated angles associated with current mutableLinkPositions
	Eigen::Vector3f tagAlongYAxis;

	float maxReach;///< determined by distance start of first link and start of the last link
	pcl::PointXYZ botFirstLink_Base;///<starting location of the first link in the chain
	pcl::PointXYZ botLastLink_Base;///<starting location of the last link in the chain
	std::vector<pcl::PointXYZ> mutableLinkEnd;

	float getLinkLength(int startJntIndex, int endJntIndex);
	void fabrikIteration(void);

	void moveEndAndToolToTarget(Eigen::Matrix4f *targFrame);

	Eigen::Matrix4f internalToolFrame;
	Eigen::Matrix4f internalGenBotEnd;

};

#endif