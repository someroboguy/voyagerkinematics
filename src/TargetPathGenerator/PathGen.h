

#include <pcl/common/common_headers.h>
const static int valuedf = 23;
#include <VoyagerMath.h>

#ifndef PATH_GEN_H
#define PATH_GEN_H

using mat4f = Eigen::Matrix4f;
typedef VoyagerMath VM;

class PathGen {

public:

	PathGen(void);
	PathGen(int val);
	mat4f getTargetFrame(void);

private:

	mat4f targetFrame;

};

#endif // !PATH_GEN_H
