#include "PathGen.h"

PathGen::PathGen(void)
{
	this->targetFrame = mat4f::Identity();
	this->targetFrame.row(3) = Eigen::RowVector4f(20, 20, 20, 1);
}

PathGen::PathGen(int val)
{
	this->targetFrame = mat4f::Identity();
	VoyagerMath::rotateAny(targetFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, -1, 0.5), M_PI * 2 / 3);


	this->targetFrame.col(0) = Eigen::RowVector4f(-0.25,-0.75,-0.612372, 0);
	this->targetFrame.col(1) = Eigen::RowVector4f(0.75,0.25,-0.612373, 0);
	this->targetFrame.col(2) = Eigen::RowVector4f(0.612372,-0.612372,0.5, 0);
	this->targetFrame.col(3) = Eigen::RowVector4f(696.985,0,430.985, 1);
	//this->targetFrame.col(3) = Eigen::RowVector4f(202, 302, 802, 1);
}


mat4f PathGen::getTargetFrame() {

	static int dir = 1;
	static int count = 0;
	static float iter = 0.0;
	static float iter2 = 0.0;
	static float iter3 = 0.0;
	static float increment = M_PI / 50;
	static float increment2 = M_PI / 25;
	static float increment3 = M_PI / 100;

	float radius1 = 300;
	float radius2 = 300;
	float radius3 = 200;
	//float x = 300 - iter;
	float x = 696.985 - radius1 + cos(iter)*radius1;
	//float y = 280 + sin(iter)*radius;
	//float x = 100;
	float y = 0 + sin(iter)*radius2;
	float z = 430.985;// -radius3 + sin(iter)*radius3;

	iter += increment;
	iter2 += increment2;

	this->targetFrame.col(3) = Eigen::RowVector4f(x, y, z, 1);

	if (count++ % 25 == 0) {
		dir *= -1;
	}
	//VoyagerMath::rotateAny(targetFrame, VM::getWAxis(&targetFrame), VM::getXAxis(&targetFrame), dir * M_PI / 500);


	//VoyagerMath::rotateAny(targetFrame, VM::getWAxis(&targetFrame), VM::getYAxis(&targetFrame), M_PI/100);

	return targetFrame;
}