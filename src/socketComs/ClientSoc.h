

#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>
#include <boost/make_shared.hpp>
#include <boost/signal.hpp>
#include <thread>
#include <iostream>
#include <string>
#include <mutex>
#include <csignal>



#ifndef SOCKETCOMS_H
#define SOCKETCOMS_H

using namespace std;
using boost::asio::ip::tcp;

class ClientSoc {

public:

	ClientSoc(char *ip, char* port);

	bool write(string msg);

	/**
	*	\brief used to read input from socket
	*	\param string * msg pointer that recieved message will be applied to.
	*	\return bool<br>
	*	&emsp;false if the connection was lost
	*	&emsp;true when the msg was received
	*
	*	This will wait indefinately for a message to be received.
	*/
	string read(bool *wasRead);
	bool isMsgAvailable(string *msg);
	void close(void);

	const string NULL_STR = "null_str"; /**< used for testing socket */


private:


	std::mutex mut;

	bool testSocket = false;
	int testScalar = CLOCKS_PER_SEC;
	int lastClockTime;

	boost::thread *t;

	//boost::thread t = boost::thread(&socketMonitor);
	boost::asio::io_service io_service;
	bool connect(const char *caller);

	boost::shared_ptr<tcp::socket>* socPtr;
	boost::shared_ptr<tcp::resolver::iterator> iteratorPtr;

	static void signal_handler(int signal);

	void socketMonitor(void);

	const int MAX_ATTEMPTS = 5;

	bool ensureConnected(void); 
	bool testSoc(void);
	string *receivedMsg;
	bool lastConnectedState;

};

#endif