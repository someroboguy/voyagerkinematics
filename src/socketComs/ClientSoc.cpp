#include "ClientSoc.h"

enum { max_length = 1024 };


/**
*	\brief used to 
*/
ClientSoc::ClientSoc(char * ip, char * port)
{
	tcp::resolver resolver(io_service);
	tcp::resolver::query query(tcp::v4(), ip, port);
	//tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

	tcp::socket* socket = new tcp::socket(io_service);
	//this->iterator = new tcp::resolver::iterator(resolver.resolve(query));
	//this->socket = socket;
	boost::shared_ptr<tcp::socket>* socPtr = new boost::shared_ptr<tcp::socket>(socket);
	boost::shared_ptr<tcp::resolver::iterator> iteratorPtr = boost::shared_ptr<tcp::resolver::iterator>(new tcp::resolver::iterator(resolver.resolve(query)));

	this->socPtr = socPtr;
	this->iteratorPtr = iteratorPtr;

	mut.lock();
	int here = 123;
	mut.unlock();

	if (this->connect(__func__)) {
		//boost::thread *tPtr = new boost::thread(socketMonitor);

		//boost::bind(&Foo::some_function, this)
	}
	boost::thread *tPtr = new boost::thread(boost::bind(&ClientSoc::socketMonitor, this));
	this->t = tPtr;
}

/**
*	\brief attempts to connect to the assigned socket. 
*	\return bool false if unable to establish socket<br>
*	&emsp;true if a connection was established. 
*/
bool ClientSoc::connect(const char *callerFunction) {

	int attempts = 0;
	lastConnectedState = false;

	while (attempts < MAX_ATTEMPTS) {
		try {
			mut.lock();
			boost::asio::connect(*(*socPtr).get(), *iteratorPtr.get());
			mut.unlock();
			lastConnectedState = true;
			return true;
		}
		catch (const std::exception& error) {
			mut.unlock();
			if (attempts == MAX_ATTEMPTS - 1) {
				// Should print the actual error message
				std::cerr << "Calling Function : " << callerFunction << " :\t Error : " << error.what() << std::endl;
			}
		}
		attempts++;
	}
	return lastConnectedState;
}

/**
*	\brief used to write the input string to this socket. 
*	\param string msg message to be put on socket. 
*	\return bool:<br>
*	&emsp;false if the message was not able to be sent<br>
*	&emsp;true if the message was able to be sent<br>
*	
*/
bool ClientSoc::write(string msg)
{
	bool cont = true;
	int attempt = 0;
		if (this->ensureConnected()) {
			try {
				mut.lock();
				boost::asio::write(*(*socPtr).get(), boost::asio::buffer(msg, msg.length()));
				mut.unlock();
				return true;
			}
			catch (const std::exception& error) {
				// Should print the actual error message
				std::cerr << error.what() << std::endl;
				lastConnectedState = false;
			}
		}
		boost::this_thread::sleep(boost::posix_time::microseconds(50));
		attempt++;
	//}
	return false;
}

string ClientSoc::read(bool * wasRead)
{
	int readAttemps = 1;



	//int numChar = this->socket->available();
	int numChar = socPtr->get()->available();

	while (numChar == 0 && readAttemps) {
		boost::this_thread::sleep(boost::posix_time::microseconds(50));
		numChar = socPtr->get()->available();
		if (this->testSocket) {
		
			this->testSocket = false;
		
			if (!testSoc()) {
				//attempting to reconnect socket. 
				if (!this->connect(__func__)) {
					*wasRead = false;
				}
			}
		}
		readAttemps++;
	}

	string myString;

	boost::asio::streambuf read_buffer;
	mut.lock();
	boost::asio::read(*(*socPtr).get(), read_buffer, boost::asio::transfer_exactly(numChar));
	mut.unlock();
	//msg << std::istream(&read_buffer);

	std::istream is(&read_buffer);
	std::string s;
	getline(is, s);
	is >> s;

	*wasRead = true;
	return s;
}

/**
*	\brief gets string in buffer if there is anything available. 
*	\param string *str updated with socket string if there is on in the buffer. 
*	\return bool<br>
*	&emsp;true string was available in socket buffer.<br>
*	&emsp;false string was NOT available in socket buffer.<br>
*
*	This does not wait for a message or determine if the socket is connected. 
*/
bool ClientSoc::isMsgAvailable(string * msg)
{
	int numChar = socPtr->get()->available();//this->socket->available();
	if (numChar != 0) {
		string myString;

		boost::asio::streambuf read_buffer;
		mut.lock();
		boost::asio::read(*(*socPtr).get(), read_buffer, boost::asio::transfer_exactly(numChar));
		mut.unlock();

		std::istream(&read_buffer) >> *msg;
		return true;
	}
	return false;
}

/**
*	\brief used to close this socket. 
*/
void ClientSoc::close(void)
{
	//this->socket->close();
	socPtr->get()->available();
}

void ClientSoc::socketMonitor(void)
{
	int perSec = CLOCKS_PER_SEC;
	int perMil = 1000 * CLOCKS_PER_SEC;
	int perTenSec = CLOCKS_PER_SEC * 10;
	int a = clock() / perTenSec;// CLOCKS_PER_SEC;//this gets the time in sec.
	int count = 0;
	int testScalar = perSec;

	while (true)
	{
		//while (clock() / CLOCKS_PER_SEC - a<1);
		while (clock() / testScalar - a < 1) {
			//
			boost::this_thread::sleep(boost::posix_time::microseconds(50));
		}
		//call your function
		a = clock() / testScalar;// CLOCKS_PER_SEC;
		//if (!this->testSoc()) {
		this->testSocket = true;



		cout << count++ << endl;
	}
}

/**
*	\brief used to ensure that there is connection. 
*	
*	when called and not connected it attempts to connect
*
*/
bool ClientSoc::ensureConnected(void)
{
	if (!lastConnectedState) {
		if (!this->connect(__func__)) {
			// if was not connected and
			// unable to connect return false. 
			return false;
		}
	}
	return true;
}

bool ClientSoc::testSoc(void)
{
	cout << "testing socket" << endl;
	//if the message was not able to be sent it is
	// assumed to have no connection. 
	if (!lastConnectedState) {
		int here = 123;
	}
	if (!this->write(NULL_STR)) { 
		lastConnectedState = false;
		return false; 
	}
	return true;
}

void ClientSoc::signal_handler(int signal)
{
	int here = 234;
}