#include "Collider.h"


Collider::Collider(float radius, pcl::PointNormal refPntNorm)
{
	this->colliderType = ColliderGeo::Sphere;
	this->radiusSquared = radius * radius;
	this->radius = radius;
	this->colliderRefPntNorm = refPntNorm;
	this->colliderRefPntNorm.normal_x = 0;
	this->colliderRefPntNorm.normal_y = 0;
	this->colliderRefPntNorm.normal_z = 0;
}

Collider::Collider(float radius, float height, bool biDirectional, pcl::PointNormal refPntNorm)
{
	this->colliderType = ColliderGeo::Cylinder;
	this->radiusSquared = radius * radius;
	this->radius = radius;
	this->heightSquared = height * height;
	this->height = height;
	this->biDirectional = biDirectional;
	if (this->biDirectional) {
		this->heightSquared *= 4;
	}
	this->colliderRefPntNorm = refPntNorm;
	this->updateCylEndPoints(this->colliderRefPntNorm);
}

bool Collider::isCollision(const pcl::PointXYZ * atPoint)
{
	switch (this->colliderType)
	{
	case ColliderGeo::Sphere:
		return this->isSphereCollision(atPoint);
		break;
	case ColliderGeo::Cylinder:
		return this->isCylinderCollision(atPoint);
		break;
	default:
		throw std::runtime_error("The collider type of this object was not accounted for in the isCollision switch");
		break;
	}

	return false;
}

bool Collider::isCollisionWithUpdate(const pcl::PointXYZ * atPoint, const pcl::PointNormal updateColliderPntNorm)
{
	manageUpdate(updateColliderPntNorm);
	return this->isCollision(atPoint);
}

bool Collider::isSphereCollision(const pcl::PointXYZ * atPoint)
{
	return VM::squareDist(atPoint, &this->colliderRefPntNorm) < this->radiusSquared ? true : false;
}

bool Collider::isCylinderCollision(const pcl::PointXYZ * atPoint)
{
	double squareDist;
	Eigen::Vector3f dirToPointOnLine(0, 0, 0);
	squareDist = VM::squareDistFromLine(atPoint, colliderRefPntNorm, &dirToPointOnLine);
	if (squareDist <= this->radiusSquared) {
		//pcl::PointXYZ pointOnLine = VM::getPointAlongLine(&dirToPointOnLine, sqrt(squareDist), *atPoint);
		pcl::PointXYZ pointOnLine = VM::getPointAlongLine(&dirToPointOnLine, sqrt(squareDist), *atPoint);
		float distToEnd = VM::squareDist(&pointOnLine, &this->clyTopCenter);
		float distToBot = VM::squareDist(&pointOnLine, &this->clyBotCenter);
		if (biDirectional) {
			if (distToEnd <= this->heightSquared && distToBot <= this->heightSquared) {
				return true;
			}
		}
		else {
			if (distToEnd <= this->heightSquared && distToBot <= this->heightSquared) {
				return true;
			}
		}
		return false;
	}
	// if the squared distance is greater than radiusSquared distance there is no collision
	return false;
}

void Collider::updateCylEndPoints(const pcl::PointNormal refPntNorm)
{
	this->colliderRefPntNorm = refPntNorm;
	this->clyBotCenter = refPntNorm;
	this->clyTopCenter = refPntNorm;

	pcl::PointXYZ newPnt = VM::getPointAlongLine(&refPntNorm, this->height);
	this->clyTopCenter.x = newPnt.x;
	this->clyTopCenter.y = newPnt.y;
	this->clyTopCenter.z = newPnt.z;

	if (biDirectional) {
		newPnt = VM::getPointAlongLine(&refPntNorm, -this->height);
		this->clyBotCenter.x = newPnt.x;
		this->clyBotCenter.y = newPnt.y;
		this->clyBotCenter.z = newPnt.z;
	}
}

void Collider::updateShperePoint(const pcl::PointNormal refPntNorm)
{
	this->colliderRefPntNorm = refPntNorm;
	this->colliderRefPntNorm.normal_x = 0;
	this->colliderRefPntNorm.normal_y = 0;
	this->colliderRefPntNorm.normal_z = 0;
}

void Collider::manageUpdate(pcl::PointNormal updateColliderPntNorm)
{
	switch (this->colliderType)
	{
	case ColliderGeo::Sphere:
		this->updateShperePoint(updateColliderPntNorm);
		break;
	case ColliderGeo::Cylinder:
		this->updateCylEndPoints(updateColliderPntNorm);
		break;
	default:
		throw std::runtime_error("The collider type of this object was not accounted for in the isCollision switch");
		break;
	}
}

