#include <VoyagerMath.h>
#include <pcl/common/common_headers.h>

#ifndef COLLIDER_H
#define COLLIDER_H

typedef VoyagerMath VM;
typedef enum {

	Sphere,
	Cylinder

}ColliderGeo;

class Collider {
public:


	/**
	*	\brief defines a spherical collider type
	*	\param float radius of spherical collider
	*	\param pcl::PointNormal point in space for collider sphere NOTE: for cylinder normal is disregarded
	*/
	Collider(float radius, pcl::PointNormal refPntNorm);
	/**
	*	\brief defines a cylinder collider type
	*	\param float radius radius of cylinder collider
	*	\param float height height of cylinder form refPntNorm
	*	\param bool biDirectional is height input considered from both directions of refPntNorm
	*	\param pcl::PointNormal position and orientation of collider
	*/
	Collider(float radius, float height, bool biDirectional, pcl::PointNormal refPntNorm);

	/**
	*	\brief was there a collision with this collider
	*	\param the point that is referenced against to determine<br>
	*	&emsp;if it has collided with this object.<br>
	*	
	*	NOTE: This may not have the most up to date position
	*	of the collider. Call this function only if you know
	*	that the collider is in the appropriate location. 
	*	call isCollisionWithUpdate() if you want to also upate
	*	this colliders frame of reference.
	*/
	bool isCollision(const pcl::PointXYZ * atPoint);
	/**
	*	\brief was there a collision with this collider
	*	\param the point that is referenced against to determine<br>
	*	&emsp;if it has collided with this object.<br>
	*
	*	NOTE: This may not have the most up to date position<br>
	*	&emsp;of the collider. Call this function only if you know<br>
	*	&emsp;that the collider is in the appropriate location.<br>
	*	&emsp;call isCollisionWithUpdate() if you want to also upate<br>
	*	&emsp;this colliders frame of reference.<br>
	*/
	bool isCollisionWithUpdate(const pcl::PointXYZ * atPoint, const pcl::PointNormal updateColliderPntNorm);
private:

	pcl::PointNormal colliderRefPntNorm;

	ColliderGeo colliderType;
	float radius = 0;			//used in parallel with radiusSquared to reduce calculations
	float radiusSquared = 0;	//used in parallel with radius to reduce calculations
	float height = 0;			//using in parallel to reduce calculation 
	float heightSquared = 0;	// used in parallel with height to reduce calculations
	bool biDirectional = false; // is cylinder height on both sides of point
	bool pendingUpdate = false;
	
	pcl::PointNormal clyBotCenter; // top center of a cylinder collider
	pcl::PointNormal clyTopCenter; // bottom center of a cylinder collider

	/**
	*	\brief used to determine if the input point is colliding with this Spherical collider object
	*	\param const pcl::PointXYZ atPoint is the point that is referenced for distance from collider refPntNorm
	*	\return returns true if there is a collision else false
	*/
	bool isSphereCollision(const pcl::PointXYZ * atPoint);

	/**
	*	\brief used to determine if the input point is colliding with this Cylindrical collider object
	*	\param const pcl::PointXYZ atPoint is the point that is referenced for distance from collider refPntNorm
	*	\return returns true if there is a collision else false
	*/
	bool isCylinderCollision(const pcl::PointXYZ * atPoint);
	/**
	*	\brief used to update the cylinder collider end points.
	*/
	void updateCylEndPoints(const pcl::PointNormal refPntNorm);

	void updateShperePoint(const pcl::PointNormal refPntNorm);
	/**
	*	\brief call anytime this colliderRefPntNorm is updateded
	*/
	void manageUpdate(pcl::PointNormal updateColliderPntNorm);

};

#endif





