
#include <pcl/common/common_headers.h>
#include <Collider.h>

#ifndef JOINT_H
#define JOINT_H

class Joint {

public:
	Joint(Eigen::Matrix4f jointFrame, float angle, float minAngle, float maxAngle, Collider collider);

	const float * getAngle(void) { return &this->currentAngle; }
	const float getAngleRef(void) { return this->currentAngle; }
	const float * getMaxAngle(void) { return &this->maxAngle; }
	const float * getMinAngle(void) { return &this->minAngle; }
	bool setNewAngle(float absoluteAngle);
	bool relativeAngMove(float relativeAngleMove);

	Eigen::Matrix4f * getFrame(void) { return &jointFrame; }
	Collider * getCollider(void) { return collider; }
	Eigen::Matrix4f getFrameRef(void) { return jointFrame; }
	void translate(const Eigen::Matrix4f bytrans);


private:
	Collider * collider;
	Eigen::Matrix4f jointFrame;
	float currentAngle;
	float maxAngle = 0;
	float minAngle = 0;


};

#endif // !JOINT_H
