#include <Joint.h>

Joint::Joint(Eigen::Matrix4f jointFrame, float angle, float minAngle, float maxAngle, Collider collider)
{
	//Eigen::Matrix4f * tempJntFrame = new Eigen::Matrix4f(jointFrame);
	this->collider = new Collider(collider);
	this->jointFrame = *new Eigen::Matrix4f(jointFrame);
	this->currentAngle = angle;
	this->maxAngle = maxAngle;
	this->minAngle = minAngle;
}

bool Joint::setNewAngle(float absoluteAngle)
{
	this->currentAngle = absoluteAngle;

	return true;
}

bool Joint::relativeAngMove(float relativeAngleMove)
{
	this->currentAngle += relativeAngleMove;
	return false;
}

void Joint::translate(const Eigen::Matrix4f bytrans)
{
	this->jointFrame(0, 3) += bytrans(0, 3);
	this->jointFrame(1, 3) += bytrans(1, 3);
	this->jointFrame(2, 3) += bytrans(2, 3);
}