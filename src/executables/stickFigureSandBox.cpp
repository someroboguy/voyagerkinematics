#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <thread>
#include <mutex>
#include <ctime>

#include <VoyagerMath.h>
#include <boost/array.hpp>
#include <boost/asio.hpp>


#include <pcl/io/vtk_lib_io.h> 
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>

float axisDist;

using namespace std;
using namespace Eigen;

void drawRefFrame(Eigen::Matrix4f frame, string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &roboview, float axisDist);



void main() {


	std::clock_t start;
	double duration;

	start = std::clock();

	/* Your algorithm here */
	//boost::this_thread::sleep(boost::posix_time::seconds(2));
	Eigen::Vector3f pntOnArbCircle(0, 0, 0);
	int i = 0;
	while(i++ < 1000){

		pcl::PointXYZ targPnt(0, 6, 3);
		pcl::PointNormal orbitPiv = pcl::PointNormal();
		orbitPiv.x = 0;
		orbitPiv.y = 0;
		orbitPiv.z = 0;
		orbitPiv.normal_x = 1;
		orbitPiv.normal_y = 0;
		orbitPiv.normal_z = 0;

		Eigen::Vector3f fromLinePntToTargPnt(0, 0, 0);
		pcl::PointXYZ pntOnLine(0, 0, 0);
		
		double rad = M_PI / 2;

		pntOnArbCircle = VoyagerMath::pntOnArbCircle(&rad, &targPnt, &orbitPiv, &fromLinePntToTargPnt);
		//pntOnArbCircle = dist * cos(rad)*fromLinePntToTargPnt + dist * sin(rad)*orbitVect.cross(fromLinePntToTargPnt) + orbitPnt;

	}

	duration = (std::clock() - start);

	std::cout << "printf: " << duration << '\n';

	pntOnArbCircle.x() = 0;
	//toolBox->at(1);

	//genB = GenericBot("iiwa.genBot");

	axisDist = 100;
	//
	//
	//
	boost::shared_ptr<pcl::visualization::PCLVisualizer> roboView(new pcl::visualization::PCLVisualizer("Robo View"));
	roboView->addCoordinateSystem(axisDist, "first");
	
	Eigen::Matrix4f targPoint = Eigen::Matrix4f::Identity();
	targPoint.col(3) = Eigen::RowVector4f(202, 202, 1202, 1);

	drawRefFrame(targPoint, "SOMETHING", roboView, axisDist);
	
	string name;
	
	//jntAngs = genB.getJntAngles();
	
	int dir = 1;
	while (!roboView->wasStopped())
	{
		//cout << "active tool frame:" << endl << *genB.getActiveToolFrame() << endl;
		roboView->spinOnce(1000, false);
		boost::this_thread::sleep(boost::posix_time::microseconds(250));
	}


	//std::thread UIThread(manageUI);
	//UIThread.join();

}



void drawRefFrame(Eigen::Matrix4f frame, string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &roboview, float axisDist) {


	pcl::ModelCoefficients xAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients yAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients zAxis = pcl::ModelCoefficients();

	//x,y,z,dx,dy,dz, angle
	//xAxis.values = std::vector<float>{ frame(0,3), frame(1,3), frame(2,3), frame(0,0), frame(1,0), frame(2,0), 10 };
	//yAxis.values = std::vector<float>{ frame(0,3), frame(1,3), frame(2,3), frame(0,1), frame(1,1), frame(2,1), 10 };
	//zAxis.values = std::vector<float>{ frame(0,3), frame(1,3), frame(2,3), frame(0,2), frame(1,2), frame(2,2), 10 };
	xAxis.values = std::vector<float>{ 0, 0, 0, axisDist * 2, 0, 0, 5 };
	yAxis.values = std::vector<float>{ 0, 0, 0, 0, axisDist * 2, 0, 5 };
	zAxis.values = std::vector<float>{ 0, 0, 0, 0, 0, axisDist * 2, 5 };

	string xName = name + "_x";

	if (!roboview->contains(xName)) {
		roboview->addCone(xAxis, xName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.5, 0, 0, xName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::ShadingRepresentationProperties::PCL_VISUALIZER_SHADING_GOURAUD, xName);
	}
	roboview->updateShapePose(xName, Eigen::Affine3f(frame));

	string yName = name + "_y";

	if (!roboview->contains(yName)) {
		roboview->addCone(yAxis, yName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0.5, 0, yName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::ShadingRepresentationProperties::PCL_VISUALIZER_SHADING_GOURAUD, yName);
	}
	roboview->updateShapePose(yName, Eigen::Affine3f(frame));


	string zName = name + "_z";

	if (!roboview->contains(zName)) {
		roboview->addCone(zAxis, zName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0.5, zName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::ShadingRepresentationProperties::PCL_VISUALIZER_SHADING_GOURAUD, zName);
	}
	roboview->updateShapePose(zName, Eigen::Affine3f(frame));


}

