#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <thread>
#include <mutex>

#include "ClientSoc.h"

#include <boost/array.hpp>
#include <boost/asio.hpp>

#include <pcl/io/vtk_lib_io.h> 
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <boost/asio.hpp>

string receivedMsg;

using namespace std;

void runSocket(void);

int main(int argc, char* argv[]){


	std::thread socThread(runSocket);

	socThread.join();
	return 0;
}

void runSocket(void) {

	ClientSoc clientSoc("127.0.0.1", "11000");

	//clientSoc.connect();

	//std::cout << "Enter message: ";

	//string input = "";
	//getline(cin, input);

	//clientSoc.write(input);
	bool wasRead = false;

	while (true) {
		receivedMsg = clientSoc.read(&wasRead);
		cout << receivedMsg << endl;
	}
	//clientSoc.close();
}