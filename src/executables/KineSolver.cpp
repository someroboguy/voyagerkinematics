#include <iostream>
#include <sstream>
#include <vector>
//#include "GenericBot.h"
#include <GenericBot.h>
#include <fstream>
#include <thread>
#include <mutex>

#include <FABRIK.h>
#include <Collider.h>

#include <boost/array.hpp>
#include <boost/asio.hpp>

#include <PathGen.h>

#include <pcl/io/vtk_lib_io.h> 
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>

using namespace std;
using namespace Eigen;

void drawRefFrame(Eigen::Matrix4f frame, string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &roboview, float axisDist);
void sandbox(void);

//void manageUI(void);

//std::mutex framesMutex;
//std::mutex framesMutex;


bool updateSLT = true;

bool updateFrameOnUI = false;
bool printFrame = false;
bool updateOnSpin = true;
bool printGoldenFrame = false;
bool printLines = true;

int numLinks;
float axisDist;




std::vector<Eigen::Matrix4f> framesList = std::vector<Eigen::Matrix4f>();
//const std::vector<Eigen::Matrix4f> * newFrames;
const std::vector<pcl::PolygonMesh> * genBMeshes;
std::vector<float> jntAngs;
const pcl::PolygonMesh * flang;
std::vector<float> angs;

int cntd = 0;

void main() {

	sandbox();

	std::vector<Tool> *tools = new std::vector<Tool>();
	GenericBot genB("iiwa.genBot", tools);
	ToolBox toolBox(tools);

	//genB.attachTool(toolBox.setActiveTool("null"));

	Tool *activeTool = toolBox.setActiveTool("null_tool");
	if (activeTool != nullptr) {
		genB.attachTool(activeTool);
	}

	FABRIK iiwaFabrik(&genB);
	//iiwaFabrik.getCurrentJointEnds();


	//toolBox->at(1);
	//genB = GenericBot("iiwa.genBot");

	axisDist = genB.getTotDist() * 0.05;
	numLinks = genB.getSize();
	genBMeshes = genB.getMeshs();

	for (int i = 0; i < numLinks; i++) {
		//if (i != 0 && i != 1) {
		if (i != 0 && i != 1) {// && i % 2 == 0){
							   //angs.push_back(M_PI / 100);
			angs.push_back(0);
		}
		else {
			angs.push_back(0);
		}
	}






	boost::shared_ptr<pcl::visualization::PCLVisualizer> roboView(new pcl::visualization::PCLVisualizer("Robo View"));
	roboView->addCoordinateSystem(axisDist * 3, "first");


	/* setting current position as known position
	*/
	//genB.moveJointAbsolute(1, -3.06182981);
	//genB.moveJointAbsolute(2, 0.339544773);
	//genB.moveJointAbsolute(3, -3.1);
	//genB.moveJointAbsolute(4, -0.92);
	//genB.moveJointAbsolute(5, 2.7834);
	//genB.moveJointAbsolute(6, 0.770504117);


	genB.moveJointAbsolute(1, 0);
	genB.moveJointAbsolute(2, 0.7853981634);
	genB.moveJointAbsolute(3, 0);
	genB.moveJointAbsolute(4, -0.7853981634);
	genB.moveJointAbsolute(5, 0);
	genB.moveJointAbsolute(6, 1.570796327);

	PathGen path_gen(23);
	Eigen::Matrix4f targPoint = path_gen.getTargetFrame();
	drawRefFrame(targPoint, "SOMETHING", roboView, axisDist);

	string name;

	if (iiwaFabrik.hitAxialSymmetricTargetLinearMove(targPoint)) {
		cout << "reachable" << endl;
	}
	else {
		cout << "NOT reachable " << endl;
	}
	//iiwaFabrik.spinTool(M_PI / 50, targPoint, true);
	
	drawRefFrame(*iiwaFabrik.getInternalToolFrame(), "iiwaTargetFrame", roboView, axisDist * 2);
	drawRefFrame(*iiwaFabrik.getInternalGenBotEnd(), "iiwaTargetEnd", roboView, axisDist);
	
	if (printLines) {
		pcl::PointXYZ endLinkB = *iiwaFabrik.getEndLinkBase();
		pcl::PointXYZ genBotEnd = VoyagerMath::getPointXYZ(iiwaFabrik.getInternalGenBotEnd());
		roboView->addLine(endLinkB, genBotEnd, "line", 0);
	
	std::vector<pcl::PointXYZ> tempLinkLinks = *iiwaFabrik.getFABRIKLinks();
	
	roboView->addLine(tempLinkLinks.at(0), tempLinkLinks.at(1), "Link1", 0);
	roboView->addLine(tempLinkLinks.at(1), tempLinkLinks.at(2), "Link2", 0);
	//iiwaFabrik.gettagAlongYAxis();
	
	int targLink = 2;
	
	//pcl::PointXYZ tempPnt(tempLinkLinks.at(targLink));
	pcl::PointXYZ tempPnt(iiwaFabrik.getGenBotEnd());
	tempPnt = VoyagerMath::getPointAlongLine(&iiwaFabrik.gettagAlongYAxis(), 500, tempPnt);
	//roboView->addLine(tempLinkLinks.at(targLink), tempPnt, "tagAlong", 0);
	roboView->addLine(iiwaFabrik.getGenBotEnd(), tempPnt, "tagAlong", 0);
	
	Eigen::Vector3f dir1 = VoyagerMath::dirToPoint(&tempLinkLinks.at(1), &tempPnt);
	Eigen::Vector3f dir2 = VoyagerMath::dirToPoint(&tempLinkLinks.at(1), &tempLinkLinks.at(1));
	
	}
	
	//angs.at(0) = iiwaFabrik.getDerivedAngle(0);// 34;
	//angs.at(1) = iiwaFabrik.getDerivedAngle(1);//2334;
	
	genB.moveJointAbsolute(1, iiwaFabrik.getDerivedAngle(0));
	genB.moveJointAbsolute(2, iiwaFabrik.getDerivedAngle(1));
	genB.moveJointAbsolute(3, iiwaFabrik.getDerivedAngle(2));
	genB.moveJointAbsolute(4, iiwaFabrik.getDerivedAngle(3));
	genB.moveJointAbsolute(5, iiwaFabrik.getDerivedAngle(4));
	genB.moveJointAbsolute(6, iiwaFabrik.getDerivedAngle(5));
	genB.moveJointAbsolute(7, iiwaFabrik.getDerivedAngle(6));

	std::vector<float> jntAngs = genB.getJntAngles();

	//jntAngs = genB.getJntAngles();

	int dir = 1;
	while (!roboView->wasStopped())
	{
		jntAngs = genB.getJntAngles();
		Eigen::Matrix4f targPoint = path_gen.getTargetFrame();
		drawRefFrame(targPoint, "SOMETHING", roboView, axisDist);

		cout << "Target Frame" << endl;
		cout << targPoint << endl;
		cout << "Frame for Joint four" << endl;
		cout << genB.getJointFrame(5) << endl;
		for (int i = 0; i < 9; i++) {
			cout << "angle" << i << ": " << jntAngs.at(i) << endl;
		}

		string name;
		
		if (iiwaFabrik.hitAxialSymmetricTargetLinearMove(targPoint)) {
		cout << "reachable" << endl;
		}
		else {
		cout << "NOT reachable " << endl;
		}
		jntAngs = genB.getJntAngles();
		//iiwaFabrik.spinTool(M_PI / 50, targPoint, true);
		
			drawRefFrame(*iiwaFabrik.getInternalToolFrame(), "iiwaTargetFrame", roboView, axisDist * 2);
			drawRefFrame(*iiwaFabrik.getInternalGenBotEnd(), "iiwaTargetEnd", roboView, axisDist);
		
		if (printLines) {
			pcl::PointXYZ endLinkB = *iiwaFabrik.getEndLinkBase();
			pcl::PointXYZ genBotEnd = VoyagerMath::getPointXYZ(iiwaFabrik.getInternalGenBotEnd());
			roboView->removeShape("line");
			roboView->addLine(endLinkB, genBotEnd, "line", 0);
		
			std::vector<pcl::PointXYZ> tempLinkLinks = *iiwaFabrik.getFABRIKLinks();
		
			roboView->removeShape("Link1");
			roboView->removeShape("Link2");
			roboView->addLine(tempLinkLinks.at(0), tempLinkLinks.at(1), "Link1", 0);
			roboView->addLine(tempLinkLinks.at(1), tempLinkLinks.at(2), "Link2", 0);
			//iiwaFabrik.gettagAlongYAxis();
		
			int targLink = 2;
		
			pcl::PointXYZ tempPnt(tempLinkLinks.at(targLink));
			//pcl::PointXYZ tempPnt(iiwaFabrik.getGenBotEnd());
			tempPnt = VoyagerMath::getPointAlongLine(&iiwaFabrik.gettagAlongYAxis(), 500, tempPnt);
			roboView->removeShape("tagAlong");
			roboView->addLine(tempLinkLinks.at(targLink), tempPnt, "tagAlong", 0);
			//roboView->addLine(iiwaFabrik.getGenBotEnd(), tempPnt, "tagAlong", 0);
		
			Eigen::Vector3f dir1 = VoyagerMath::dirToPoint(&tempLinkLinks.at(1), &tempPnt);
			Eigen::Vector3f dir2 = VoyagerMath::dirToPoint(&tempLinkLinks.at(1), &tempLinkLinks.at(1));
		
		}
		
		//angs.at(0) = iiwaFabrik.getDerivedAngle(0);// 34;
		//angs.at(1) = iiwaFabrik.getDerivedAngle(1);//2334;
		
		genB.moveJointAbsolute(1, iiwaFabrik.getDerivedAngle(0));
		genB.moveJointAbsolute(2, iiwaFabrik.getDerivedAngle(1));
		genB.moveJointAbsolute(3, iiwaFabrik.getDerivedAngle(2));
		genB.moveJointAbsolute(4, iiwaFabrik.getDerivedAngle(3));
		genB.moveJointAbsolute(5, iiwaFabrik.getDerivedAngle(4));
		genB.moveJointAbsolute(6, iiwaFabrik.getDerivedAngle(5));
		genB.moveJointAbsolute(7, iiwaFabrik.getDerivedAngle(6));
		
		

		jntAngs = genB.getJntAngles();
		if (updateOnSpin) {

			if (cntd % 50 == 0) {
				dir *= -1;
				for (int i = 0; i < angs.size(); i++) {
					angs.at(i) *= dir;
				}
				//jntAngs = genB.getJntAngles();
			}

			//genB.moveJointsAbsolute(angs);
			//genB.moveJointRelative(3, M_PI / 4);
			//genB.moveJointAbsolute(3, M_PI / 4);
			//genB.moveJointsAbsolute(angs);
			//genB.moveJointsRelative(angs);

			if (updateSLT) {
				genBMeshes = genB.getMeshs();
				for (int i = 0; i < numLinks; i++) {
					if (i == 4) {
						int here = 234;
					}
					string poly = "poly";
					poly.append(to_string(i));


					//Eigen::Affine3f dbEigen = Eigen::Affine3f(*genB.getJointFrame(i));// newFrames->at(i));
					Eigen::Affine3f dbEigen = Eigen::Affine3f(*genB.joints.at(i).getFrame());// newFrames->at(i));
					if (genBMeshes->at(i).polygons.size() != 0 && !roboView->updatePolygonMesh(genBMeshes->at(i), poly)) {
						if (i != 8) {
							roboView->addPolygonMesh(genBMeshes->at(i), poly);
						}
					}
				}
			}

			if (updateFrameOnUI) {
				for (int i = 0; i < numLinks; i++) {
					name = "frame";
					name.append(std::to_string(i));
					//drawRefFrame(*genB.getJointFrame(i), name, roboView, axisDist);
					drawRefFrame(*genB.joints.at(i).getFrame(), name, roboView, axisDist);
				}

				drawRefFrame(*genB.getActiveToolFrame(), "activeTool", roboView, axisDist);
			}
			cout << *genB.getActiveToolFrame() << endl;
		}

		cntd++;
		//cout << "active tool frame:" << endl << *genB.getActiveToolFrame() << endl;
		roboView->spinOnce(10);
		//boost::this_thread::sleep(boost::posix_time::microseconds(50));
	}


	//std::thread UIThread(manageUI);
	//UIThread.join();

}


void drawRefFrame(Eigen::Matrix4f frame, string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &roboview, float axisDist) {


	pcl::ModelCoefficients xAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients yAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients zAxis = pcl::ModelCoefficients();

	//x,y,z,dx,dy,dz, angle
	//xAxis.values = std::vector<float>{ frame(0,3), frame(1,3), frame(2,3), frame(0,0), frame(1,0), frame(2,0), 10 };
	//yAxis.values = std::vector<float>{ frame(0,3), frame(1,3), frame(2,3), frame(0,1), frame(1,1), frame(2,1), 10 };
	//zAxis.values = std::vector<float>{ frame(0,3), frame(1,3), frame(2,3), frame(0,2), frame(1,2), frame(2,2), 10 };
	xAxis.values = std::vector<float>{ 0, 0, 0, axisDist * 2, 0, 0, 5 };
	yAxis.values = std::vector<float>{ 0, 0, 0, 0, axisDist * 2, 0, 5 };
	zAxis.values = std::vector<float>{ 0, 0, 0, 0, 0, axisDist * 2, 5 };

	string xName = name + "_x";

	if (!roboview->contains(xName)) {
		roboview->addCone(xAxis, xName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.5, 0, 0, xName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::ShadingRepresentationProperties::PCL_VISUALIZER_SHADING_GOURAUD, xName);
	}
	roboview->updateShapePose(xName, Eigen::Affine3f(frame));

	string yName = name + "_y";

	if (!roboview->contains(yName)) {
		roboview->addCone(yAxis, yName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0.5, 0, yName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::ShadingRepresentationProperties::PCL_VISUALIZER_SHADING_GOURAUD, yName);
	}
	roboview->updateShapePose(yName, Eigen::Affine3f(frame));


	string zName = name + "_z";

	if (!roboview->contains(zName)) {
		roboview->addCone(zAxis, zName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0.5, zName);
		roboview->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::ShadingRepresentationProperties::PCL_VISUALIZER_SHADING_GOURAUD, zName);
	}
	roboview->updateShapePose(zName, Eigen::Affine3f(frame));


}

void sandbox(void)
{
	pcl::PointXYZ atPoint(2,2,2);
	pcl::PointNormal fromLine = pcl::PointNormal();
	fromLine.x = 0;
	fromLine.y = 0;
	fromLine.z = 0;
	fromLine.normal_x = 1;
	fromLine.normal_y = 1;
	fromLine.normal_z = 0;
	Eigen::Vector3f dirToPointOnLine(0, 0, 0);
	double someVal = VoyagerMath::squareDistFromLine(&atPoint, fromLine, &dirToPointOnLine);

	pcl::PointXYZ pointOnLine = VoyagerMath::getPointAlongLine(&dirToPointOnLine, sqrt(someVal), atPoint);

	someVal++;

	pcl::PointNormal location = pcl::PointNormal();
	location.x = 0;
	location.y = 0;
	location.z = 0;
	location.normal_x = 0;
	location.normal_y = 0;
	location.normal_z = 1;


	Collider collider(3, 10, true, location);
	bool wasCollidion = collider.isCollision(&pcl::PointXYZ(4, 0, 0));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(0, 1, 0));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(3, 0, 0));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(2, 0, 0));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(1, 1, 11));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(3, 4, -9));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(2, 0, -10));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(0, 2, -11));
	wasCollidion = collider.isCollision(&pcl::PointXYZ(0, 0, 0));


}
